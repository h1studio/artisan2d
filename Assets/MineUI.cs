﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MineUI : MonoBehaviour
{
    [SerializeField]
    private Button btn_Mine;
    [SerializeField]
    private Button btn_OpenWorker;

    [SerializeField]
    private Button btn_PreviousMine;
    [SerializeField]
    private Button btn_NextMine;

    [SerializeField]
    private Text[] text_OreAmount;

    private void Start()
    {
        btn_Mine.onClick.AddListener(OnClick_MineBtn);
        btn_OpenWorker.onClick.AddListener(OnClick_OpenWorkerBtn);

        btn_PreviousMine.onClick.AddListener(delegate { MineManager.instance.MoveNextMine(-1); });
        btn_NextMine.onClick.AddListener(delegate { MineManager.instance.MoveNextMine(1); });

        ObjectManager.instance.player.OnOreChanged += UpdateUI_OreAmount;
    }

    private void OnClick_MineBtn()
    {
        MineManager.instance.CurMine.stone.Mining(ObjectManager.instance.player.statCollection);
    }
    //todo: Panel관리자에서 관리하기
    private void OnClick_OpenWorkerBtn()
    {
        ObjectManager.instance.workerUI.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    private void UpdateUI_OreAmount(OreCollection pOreCollection)
    {
        for(int i=0; i < text_OreAmount.Length; i++)
        {
            text_OreAmount[i].text = pOreCollection.GetOreAmount(DefinitionList_Database.instance.GetOreByID(i)).ToString();
        }
    }
}


