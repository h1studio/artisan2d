﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUI : MonoBehaviour {


    [SerializeField]
    private Button btn_Mine;
    [SerializeField]
    private Button btn_Furnace;
    [SerializeField]
    private Button btn_Forging;

    [SerializeField]
    private Button btn_Inventory;

    private void Start()
    {
        btn_Mine.onClick.AddListener(PanelManager.instance.OpenMinePanel);
        btn_Furnace.onClick.AddListener(PanelManager.instance.OpenSmeltPanel);
        btn_Forging.onClick.AddListener(PanelManager.instance.OpenForgingPanel);
        btn_Inventory.onClick.AddListener(PanelManager.instance.OpenInventory);
    }

}
