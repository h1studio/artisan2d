﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface IStat
{    
    float ValueRaw { get; }
    float Value { get; }
    float ValueNotClamped { get; }

    float LocalFactor { get; }
    float GlobalFactor { get; }

    int Level { get; set; }
    float LevelIndex { get; }

    void AddStat(IStat pStat);
    void RemoveStat(IStat pStat);

    void AddStat(float pValue, float pLocalFactor = 1f, float pGlobalFactor = 1f);

    //event Action<Stat> OnStatChanged;

    //void Notify_StatChanged();
}
