﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderSelectUI : MonoBehaviour {

    private Order order;

    [SerializeField] private Button btn_Deliver;
    [SerializeField] private Button btn_GiveUp;
    [SerializeField] private Button btn_SelectItem;

    private void Start()
    {
        Debug.Log("123");
        btn_Deliver.onClick.AddListener(OnClick_Deliver);
        btn_GiveUp.onClick.AddListener(OnClick_GiveUp);
        //btn_SelectItem.onClick.AddListener(delegate { OrderManager.Deliver(order); });
    }

    private void OnClick_Deliver()
    {
        OrderManager.Deliver(order);
    }
    private void OnClick_GiveUp()
    {
        OrderManager.GiveUp(order);
    }
    private void OnClick_SelectItem()
    {

    }
}
