﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tesseract;
using UnityEngine.UI;

public class Dragon : MonoBehaviour {
    
    // 1초에 1Gage가 참
    public float maxGage = 7;

    private Timer gageTimer;
    
    [SerializeField]
    private Anvil anvil;

    [SerializeField]
    private Slider gageSlider;
    [SerializeField]
    private Button btn;

    private void Awake()
    {
        gageTimer = gameObject.AddComponent<Timer>();
        gageTimer.Init(maxGage, null,UpdateUI_GageTimer);

        btn.onClick.AddListener(Fire);
        gageSlider.maxValue = maxGage;
    }

    public void Fire()
    {
        // 타이머 진행중인지 확인
        if (gageTimer.isTimerRunning)
        {
            Debug.Log("재사용 대기시간 입니다.");
            return;            
        }

        // 타이머 시작
        gageTimer.StartTimer();

        anvil.subTemperature.ChangeValue(300f);
        Debug.Log("Fire");
        //    // 브레스 발사
        //    ForgingManager.GetInstance().GetAnvil().IncreaseTemperature(breathPower);
        //    // 용 입벌리기
        //    dragonObject.GetComponent<Image>().sprite = dragonImage_fire;
        //    StartCoroutine(EndFire());
        //    // 애니메이션
        //    successCastingParticle.SetActive(true);
        //    // 소리
        //    GetComponent<SoundController>().Play();
    }

    private void UpdateUI_GageTimer()
    {
        gageSlider.value = gageTimer.Value;
    }
}
