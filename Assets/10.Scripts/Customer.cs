﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;
using UnityEngine.UI;

public class Customer : MonoBehaviour {
    
    [SerializeField]
    private CustomerDefinition customerDefinition;

    public string Name { get { return customerDefinition.Name; } }

    private CustomerState _state;
    public CustomerState State
    {
        get { return _state; }
        private set { _state = value; Notify_StateChanged(); }
    }

    private Order _order;
    public Order Order
    {
        get { return _order; }
        private set { _order = value; }
    }

    [SerializeField]
    private OrderSheet orderSheet;
    [SerializeField]
    private GameObject orderSelectUI;

    [SerializeField]
    private float timeToPosition;
    private Coroutine movingCoroutine;

    [SerializeField]
    private Transform customerStartPosition;    // 손님 시작 위치

    public event Action<CustomerState> OnStateChanged;

    private void Start()
    {
        OnStateChanged += UpdateUI_StateChanged;

        // UI
        GetComponent<Button>().onClick.AddListener(OnClick_Customer);
        //ObjectManager.instance.orderUI.OnCustomerChanged += UpdateUI_CustomerChanged;
    }

    public void ShowOrder()
    {
        if (State == CustomerState.Enter)
        {
            State = CustomerState.Order;        // 손님 상태 변경

            Order = transform.gameObject.AddComponent<Order>();     // 주문 생성
            Order.Init(customerDefinition.GetOrder());     // 주문 초기화   
            orderSheet.SetOrder(Order);

            Debug.Log(Name + "의 주문 : " + Order.Item.Name);
        }
    }
    public void Rest()
    {
        State = CustomerState.Rest;
    }

    public void MovePosition(Vector3 pDestination, UnityAction pAction = null)
    {
        if (movingCoroutine != null)
            StopCoroutine(movingCoroutine);

        if (State == CustomerState.Enter)
            pAction = ShowOrder;
        movingCoroutine = StartCoroutine(TS.MoveObjectByTime(gameObject, transform.position, pDestination, timeToPosition, pAction));
    }

    public void EnterShop(Vector3 pDestination)
    {
        State = CustomerState.Enter;        // 손님 상태 변경

        MovePosition(pDestination, ShowOrder);      // 손님 이동, 이동 완료 후 ShowOrder() 실행    
    }
    public void OutShop()
    {
        State = CustomerState.Return;

        MovePosition(customerStartPosition.position, Rest);
    }

    private void Notify_StateChanged()
    {
        if (OnStateChanged != null)
            OnStateChanged(State);
    }
    public void ToggleOrderSelectUI(bool b)
    {
        orderSelectUI.SetActive(b);
    }


    #region UI
    private void OnClick_Customer()
    {
        if(State == CustomerState.Order)
        {
            ObjectManager.instance.shop.SetCurCustomer(this);
            ToggleOrderSelectUI(true);
            //ObjectManager.instance.orderUI.SetCustomer(this);
        }
    }

    private void UpdateUI_StateChanged(CustomerState pState)
    {
        if(pState == CustomerState.Order)
            orderSheet.gameObject.SetActive(true);
        else if(pState == CustomerState.Return)
        {
            orderSheet.gameObject.SetActive(false);
            ToggleOrderSelectUI(false);
        }
                  
    }
    private void UpdateUI_CustomerChanged(Customer pCustomer)
    {
        if (pCustomer == this)
        {
            orderSheet.UpdateUI_Color(true);
        }
        else
        {
            orderSheet.UpdateUI_Color(false);
        }
    }
    #endregion
}
