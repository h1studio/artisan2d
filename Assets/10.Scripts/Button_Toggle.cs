﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button_Toggle : ButtonSetting {

    private bool isActive;

    protected override void OnClick_Button()
    {
        base.OnClick_Button();

        if(!isActive)
        {
            // 패널 열기
            ActivePanel(true);

            // 버튼 색깔 변경
            SetButtonColor(changedColor);

            isActive = true;
        }
        else
        {
            ResetButton();

            isActive = false;
        }
    }
}
