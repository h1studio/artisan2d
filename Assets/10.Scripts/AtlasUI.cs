﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AtlasUI : MonoBehaviour {

    [SerializeField]
    private Atlas atlas;
    //[SerializeField]
    //private Sprite atlasImage;

    //[SerializeField]
    //private Image atlas;

    private int atlasDivisionCount = 3;
    private int atlasSizeLevel = 1;

    public int width { get { return Screen.width / atlasSizeLevel; } }
    public int height { get { return Screen.height / atlasSizeLevel; } }

    [SerializeField]
    private Button btn_SetSchedule;
    
    private void Start()
    {
        //atlas.sprite = atlasImage;


        SetAtlasSize(3);

        MoveScreen(0, 0);
        Debug.Log("Move");

        btn_SetSchedule.onClick.AddListener(ToggleScheduleMode);

        //atlas.Delegate_UpdateUI_Schedule += UpdateUI_Schedule;
    }

    //private void UpdateUI_Schedule(List<MapInfo> pMapInfoList)
    //{
    //    foreach(MapInfo map in pMapInfoList)
    //    {
    //        map.mapUI.
    //    }
    //}

    public void ToggleScheduleMode()
    {
        if (Atlas.scheduleMode)
            Atlas.scheduleMode = false;
        else
            Atlas.scheduleMode = true;
    }


    //todo : 맵아이콘 크기도 변경되도록 수정
    // 1 : 1*1보기, 2 : 2*2보기, 3 : 3*3보기(전체 보기) 
    public void SetAtlasSize(int patlasSizeLevel)
    {
        atlasSizeLevel = patlasSizeLevel;
        Vector2 sizeVector = new Vector2(Screen.width, Screen.height) * atlasDivisionCount / patlasSizeLevel;
        GetComponent<RectTransform>().sizeDelta = sizeVector;
    }

    /// <summary>
    /// x,y 평면 좌표계 ex. (0,0), (3,2)
    /// </summary>
    /// <param name="column"></param>
    /// <param name="row"></param>
    public void MoveScreen(int x, int y)
    {
        transform.localPosition = new Vector2(-width * x, -height * y);
    }

    [ContextMenu("00")]
    public void AA()
    {
        MoveScreen(0, 0);
    }
    [ContextMenu("11")]
    public void AAa()
    {
        MoveScreen(1, 1);
    }
}
