﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInfo : MonoBehaviour {

    public EnemyDefinition enemyDefinition;

    public StatCollection statCollection;
    public Status status;

    public string Name { get { return enemyDefinition.Name; } }

    public EnemyInfo(EnemyDefinition pEnemy)
    {
        enemyDefinition = pEnemy;
        status = new Status(pEnemy.status);
        statCollection.AddStatus(status);
    }

}
