﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : ManagerBase<Tester>
{

    [SerializeField]
    private Transform chatParent;

    public void AddOre(int pAmount)
    {
        OreInfo tOreInfo = new OreInfo("구리 광석", pAmount);
        ObjectManager.instance.player.AddOre(tOreInfo);
    }

    public void ProduceItem(string pItemName)
    {
        ItemInfo tItemInfo = new ItemInfo(pItemName);
        ObjectManager.instance.player.inventory.AddItem(tItemInfo);
    }

    public void AddGeneralChat(string pContent)
    {
        var chat = new Chat(ObjectManager.instance.player, pContent);

        GameObject prefab_Chat = (GameObject)Resources.Load("Prefabs/GeneralChat");
        var chatUI = Instantiate(prefab_Chat);
        chatUI.transform.SetParent(chatParent);
        chatUI.GetComponent<ChatUI>().SetChat(chat);
    }
    public void AddMyChat(string pContent)
    {
        var chat = new Chat(ObjectManager.instance.player, pContent);

        GameObject prefab_Chat = (GameObject)Resources.Load("Prefabs/MyChat");
        var chatUI = Instantiate(prefab_Chat);
        chatUI.transform.SetParent(chatParent);
        chatUI.GetComponent<ChatUI>().SetChat(chat);
    }
    public void AddQuestChatRoom()
    {
        ChatManager.instance.MakeQuestChatRoom();
    }
    public void AddAdventureChatRoom()
    {
        ChatManager.instance.MakeAdventureChatRoom();
    }

    public void SetTemperature(float pValue)
    {
        SmeltManager.instance.CurFurnace.temperature.SetValue(pValue);
    }
    public void SetMeltingRatio(float pValue)
    {
        SmeltManager.instance.CurFurnace.meltingRatio.SetValue(pValue);
    }
    public void SetGameSpeed(int pValue)
    {
        Time.timeScale = pValue;
    }
}
