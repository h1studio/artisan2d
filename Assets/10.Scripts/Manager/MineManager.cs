﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tesseract;
using System;

public class MineManager : ManagerBase<MineManager> {

    private Mine _curMine;
    public Mine CurMine { get { return _curMine; } protected set { _curMine = value; } }
    public Transform workerRestPosition;

    [SerializeField]
    private Mine[] mineList;

    public void Awake()
    {
        MoveToMine(mineList[0]);
    }

    public void MoveToMine(Mine pMine)
    {
        CurMine = pMine;

        //todo: 카메라 이동
        // 메인카메라 이동
        // 일꾼 카메라 이동
        Notify_CurMineChanged();
    }
    public void MoveNextMine(int pMove)
    {
        var curMineIndex = -1;
        for (int i=0; i < mineList.Length; i++)
        {
            if (mineList[i] == CurMine)
                curMineIndex = i;
        }

        MoveToMine(mineList[Mathf.Clamp(curMineIndex + pMove, 0, mineList.Length - 1)]);
    }

    public event Action<Mine> OnMineChanged;

    private void Notify_CurMineChanged()
    {
        if (OnMineChanged != null)
            OnMineChanged(CurMine);
    }
}
