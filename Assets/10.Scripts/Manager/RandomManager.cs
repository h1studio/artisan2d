﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomManager : MonoBehaviour {

    /// <summary>
    /// pMax부터 시작하여 1까지 각 ratio의 확률로 해당값 리턴.
    /// 1까지 리턴이 안됬을 시, runUntilNotZero = True이면 pMax부터 재실행, False이면 0리턴 
    /// pMax와 ratio의 크기가 같아야 함
    /// </summary>
    /// <param name="pMax"></param>
    /// <param name="ratio"></param>
    /// <param name="runUntilNotZero"></param>
    public static int GetTierByRandom(List<double> ratio, List<int> picks, bool runUntilNotZero = false)
    {
        //if(pMax != ratio.Count)
        //{
        //    Debug.LogWarning("인덱스가 맞지 않습니다.\n pMax : " + pMax + "\nratio.count : " + ratio.Count);
        //    return 0;
        //}

        int breakNum = 1000;
        while(breakNum > 0)
        {
            for (int i = ratio.Count; i > 0; i--)
            {
                if (!picks.Contains(i))
                    continue;

                double dice = Random.value;
                if (dice < ratio[i - 1] / 100d)
                {
                    return i;
                }
            }

            if (!runUntilNotZero)
                return 0;
            else
                breakNum--;
        }
        Debug.LogWarning("1000번이나 돌려도 선택이 안됬습니다. 뭔가 이상..");
        return 0;
    }

    public static T GetVarByRandom<T>(List<T> pList)
    {
        if (pList.Count != 0)
        {
            return pList[Random.Range(0, pList.Count)];
        }
        return default(T);
    }
}
