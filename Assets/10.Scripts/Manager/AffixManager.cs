﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AffixManager : ManagerBase<AffixManager> {

    public List<double> craftRatio;
    public List<double> engraveRatio;

    public void SetFirstAffixOnItem(ItemInfo pItem, ItemInfo pMaterialItem)
    {
        var affixList = pMaterialItem.PossibleAffixList;

        AffixDefinition a = RandomManager.GetVarByRandom(affixList);
        pItem.SetAffix(a);
        Debug.Log("'" + pItem.Name + "'에 '" + a.Name + "' Affix 적용 완료");
    }
    public void SetSecondAffixOnItem(ItemInfo pItem, ItemInfo pMaterialItem)
    {
        var affixList = pMaterialItem.PossibleAffixList;
        var tierList = new List<int>();
        foreach(AffixDefinition affix in affixList)
        {
            if (!tierList.Contains(affix.tier))
                tierList.Add(affix.tier);
        }

        var tier = RandomManager.GetTierByRandom(craftRatio,tierList,true);
                
        AffixDefinition a = GetAffixByTier(affixList, tier);
        pItem.SetAffix(a);
        Debug.Log("'" + pItem.Name + "'에 '" + a.Name + "' Affix 적용 완료");
    }
    public void SetThirdAffixOnItem(ItemInfo pItem)
    {
        var affixList = DefinitionList_Database.instance.GetAffixListByType(AffixType.Engrave);
        var tierList = new List<int>();
        foreach (AffixDefinition affix in affixList)
        {
            if (!tierList.Contains(affix.tier))
                tierList.Add(affix.tier);
        }

        var tier = RandomManager.GetTierByRandom(engraveRatio, tierList, false);

        AffixDefinition a = GetAffixByTier(affixList, tier);
        pItem.SetAffix(a);
        Debug.Log("'" + pItem.Name + "'에 '" + a.Name + "' Affix 적용 완료");
    }

    //float GetPurityBonusRate(float pPurity)
    //{
    //    float tPurity = pPurity / 100f;
    //    if (tPurity >= 0.5)
    //    {
    //        return (Mathf.Pow(tPurity * 10 - 5, 2) * 4 / 25) + 1;   // 순도가 100이면 최대 5배로 확률 상승. 2차곡선
    //    }
    //    else
    //    {
    //        return 1f;
    //    }
    //}
    
    private AffixDefinition GetAffixByTier(List<AffixDefinition> pAffixList, int pTier)
    {
        if (pTier == 0)
        {
            Debug.LogWarning("티어가 0이면 어떡행");
            return null;
        }

        List<AffixDefinition> affixList = new List<AffixDefinition>();
        foreach (AffixDefinition a in pAffixList)
        {
            if (a.tier == pTier)
            {
                affixList.Add(a);
            }
        }

        return RandomManager.GetVarByRandom(affixList);
    }
}
