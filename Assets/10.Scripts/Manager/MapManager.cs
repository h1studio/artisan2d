﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : ManagerBase<MapManager> {

    private List<MapInfo> mapList = new List<MapInfo>();

    private void Awake()
    {
        foreach(MapDefinition mapDefinition in DefinitionList_Database.instance.mapList)
        {
            MapInfo map = new MapInfo(mapDefinition);
            mapList.Add(map);
        }        
    }


    public void AddMapList(MapInfo pMap)
    {
        mapList.Add(pMap);
    }

    public MapInfo GetMapByName(string pName)
    {
        foreach (MapInfo t in mapList)
        {
            if (t.Name == pName)
            {
                return t;
            }
        }
        return null;
    }
    public MapInfo GetMapByDefinition(MapDefinition pMap)
    {
        foreach (MapInfo t in mapList)
        {
            if (t.mapDefinition == pMap)
            {
                return t;
            }
        }
        return null;
    }
}
