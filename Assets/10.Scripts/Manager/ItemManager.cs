﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : ManagerBase<ItemManager> {

    public ItemInfo ProduceItem(FrameDefinition pFrame, OreDefinition pOre)
    {
        foreach (ItemDefinition item in DefinitionList_Database.instance.itemList)
        {
            if (item.CanProduce)
            {
                IProducable produceItem = item.producable;
                if(produceItem.CheckToProduce(pFrame, pOre))
                {
                    return produceItem.ProduceItem(item);
                }
            }
        }
        return null;
    }
}
