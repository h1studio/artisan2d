﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerManager : ManagerBase<CustomerManager> {

    private Customer[] customerList;

    private void Awake()
    {
        customerList = FindObjectsOfType<Customer>();
    }

    //public Customer GetCustomerByDefinition(CustomerDefinition pCustomer)
    //{
    //    foreach (Customer t in customerList)
    //    {
    //        if (t.customerDefinition == pCustomer)
    //        {
    //            return t;
    //        }
    //    }
    //    return null;
    //}
    
    public Customer GetRestCustomerByRandom()
    {
        List<Customer> tList = new List<Customer>();
        foreach(Customer customer in customerList)
        {
            if (customer.State == CustomerState.Rest)
            {
                tList.Add(customer);
            }
        }

        return tList[Random.Range(0, tList.Count)];
    }


}
