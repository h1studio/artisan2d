﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : ManagerBase<UIManager> {

    public Color copperColor;
    public Color ironColor;
    public Color silverColor;
    public Color goldColor;
}
