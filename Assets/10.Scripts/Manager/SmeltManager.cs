﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SmeltManager : ManagerBase<SmeltManager> {
    
    private Furnace _curFurnace;
    public Furnace CurFurnace { get { return _curFurnace; } protected set { _curFurnace = value; } }

    [SerializeField]
    private Furnace[] furnaceList;
    
    public void Awake()
    {
        MoveToMine(furnaceList[0]);
    }

    public void MoveToMine(Furnace pFurnace)
    {
        CurFurnace = pFurnace;

        //todo: 카메라 이동
        // 메인카메라 이동
        Notify_CurFurnaceChanged();
    }
    public void MoveNextMine(int pMove)
    {
        var curFurnaceIndex = -1;
        for (int i = 0; i < furnaceList.Length; i++)
        {
            if (furnaceList[i] == CurFurnace)
                curFurnaceIndex = i;
        }

        MoveToMine(furnaceList[Mathf.Clamp(curFurnaceIndex + pMove, 0, furnaceList.Length - 1)]);
    }

    public event Action<Furnace> OnFurnaceChanged;

    private void Notify_CurFurnaceChanged()
    {
        if (OnFurnaceChanged != null)
            OnFurnaceChanged(CurFurnace);
    }

}
