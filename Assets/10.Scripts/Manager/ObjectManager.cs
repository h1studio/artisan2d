﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : ManagerBase<ObjectManager> {

    public Player player;
    public Atlas father;

    [Header("UI Panel")]
    public WorkerUI workerUI;
    public InventoryUI inventoryUI;
    public MapSelectUI mapSelectUI;
    public Shop shop;
    public MineManager mineUI;

    public Transform chatUIParent;
    public Transform chatRoomUI;

    public SystemChat systemChat;
    
}
