﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WorkerManager : ManagerBase<WorkerManager> {
    
    private List<Worker> workerList = new List<Worker>();
    [SerializeField]
    public Transform workerParent;

    public void AddWorker()
    {
        var workerPrefab = (GameObject)Resources.Load("Prefabs/Worker");
        var worker = Instantiate(workerPrefab);
        worker.transform.SetParent(workerParent);
        workerList.Add(worker.GetComponent<Worker>());

        Notify_WorkerListChanged();
    }

    public event Action<List<Worker>> OnWorkerListChanged;

    public void Notify_WorkerListChanged()
    {
        if (OnWorkerListChanged != null)
            OnWorkerListChanged(workerList);
    }
}
