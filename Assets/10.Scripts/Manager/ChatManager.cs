﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatManager : ManagerBase<ChatManager> {

    [SerializeField]
    private Transform questChatRoomParent;
    [SerializeField]
    private Transform adventureChatRoomParent;


    private List<ChatRoom> questChatRoomList = new List<ChatRoom>();
    private List<ChatRoom> adventureChatRoomList = new List<ChatRoom>();
    public static ChatRoom curChatRoom;


    //public ChatRoom MakeChatRoom(int pIndex)
    //{
    //    if(pIndex == 0)
    //    {
    //        return MakeQuestChatRoom();
    //    }
    //    else
    //    {
    //        return MakeAdventureChatRoom();
    //    }
    //}

    public ChatRoom MakeQuestChatRoom()
    {
        var chatRoom = (GameObject)Resources.Load("Prefabs/QuestChatRoom");
        var containerObject = Instantiate(chatRoom);
        containerObject.transform.SetParent(questChatRoomParent);

        var chat = containerObject.GetComponent<ChatRoom>();
        questChatRoomList.Add(chat);

        curChatRoom = chat;

        return chat;
    }
    public ChatRoom MakeAdventureChatRoom()
    {
        var chatRoom = (GameObject)Resources.Load("Prefabs/AdventureChatRoom");
        var containerObject = Instantiate(chatRoom);
        containerObject.transform.SetParent(adventureChatRoomParent);

        var chat = containerObject.GetComponent<ChatRoom>();
        adventureChatRoomList.Add(chat);

        curChatRoom = chat;

        return chat;
    }
    public void RemoveChatRoom(ChatRoom pChatRoom)
    {
        foreach(ChatRoom c in adventureChatRoomList)
        {
            if(c == pChatRoom)
            {
                Destroy(c.gameObject);
                adventureChatRoomList.Remove(c);
            }
        }
    }

    public void OpenChatRoom()
    {
        ObjectManager.instance.chatRoomUI.gameObject.SetActive(true);
    }
}
