﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class Timer : MonoBehaviour{
    
    public UnityAction timerChanged;
    public UnityAction timerEnd;
    
    public float MaxValue { get; private set; }
    public float Value { get; private set; }

    public bool isTimerRunning = false;

    public void Init(float pMaxTime, UnityAction pTimerEnd, bool pDestroy = false)
    {
        MaxValue = pMaxTime;

        if (pTimerEnd != null)
            timerEnd = pTimerEnd;

        if (pDestroy)
            timerEnd += Destroy;
    }
    public void Init(float pMaxTime, UnityAction pTimerEnd, UnityAction pTimerChanged)
    {
        Init(pMaxTime, pTimerEnd);

        if (pTimerChanged != null)
            timerChanged = pTimerChanged;
    }

    public void AddAction_TimerChanged(UnityAction pTimerChanged)
    {
        if (pTimerChanged != null)
            timerChanged += pTimerChanged;
    }

    public void StartTimer()
    {
        isTimerRunning = true;
        StartCoroutine(RunTimer());
    }

    public void StopTimer()
    {
        isTimerRunning = false;
        StopAllCoroutines();
        Notify_TimerChanged();
    }

    private IEnumerator RunTimer()
    {        
        Value = 0f;
        while (Value < MaxValue)
        {
            Value += Time.deltaTime;

            Notify_TimerChanged();
            yield return null;
        }
        isTimerRunning = false;

        Notify_TimerChanged();
        Notify_TimerEnd();
    }

    private void Destroy()
    {
        Destroy(this);
    }

    private void Notify_TimerChanged()
    {
        if (timerChanged != null)
            timerChanged();
    }
    private void Notify_TimerEnd()
    {
        if (timerEnd != null)
            timerEnd();
    }
}
