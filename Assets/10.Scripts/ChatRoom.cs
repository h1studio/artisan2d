﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tesseract;

[RequireComponent(typeof(Button))]
public class ChatRoom : MonoBehaviour {

    public List<Chat> chatList = new List<Chat>();
    
    private Transform chatUIParent;

    public List<IChattableObject> chatPerson = new List<IChattableObject>();
    

    // UI Variables
    [Required, SerializeField]
    private Text text_Label;
    [Required, SerializeField]
    private Text text_Person;
    [Required, SerializeField]
    private Text text_Content;

    private GameObject prefab_Chat;

    public void AddChat(Chat pChat)
    {
        chatList.Add(pChat);

        if(pChat.ChatType == ChatType.System)
            chatPerson.Add(pChat.chatPerson);
        
        if(ChatManager.curChatRoom == this)
        {
            MakeChatUI(pChat);
        }

        Notify_ChatChanged();
    }
    private void MakeChatUI(Chat pChat)
    {
        if (pChat.ChatType == ChatType.General)
            prefab_Chat = (GameObject)Resources.Load("Prefabs/GeneralChat");
        else if (pChat.ChatType == ChatType.Mine)
            prefab_Chat = (GameObject)Resources.Load("Prefabs/MyChat");
        else if (pChat.ChatType == ChatType.System)
            prefab_Chat = (GameObject)Resources.Load("Prefabs/SystemChat");

        var chatUI = Instantiate(prefab_Chat);
        chatUI.transform.SetParent(chatUIParent);
        chatUI.GetComponent<ChatUI>().SetChat(pChat);
    }

    public void Notify_ChatChanged()
    {
        UpdateUI_Chat();
    }


    // UI
    private void Start()
    {
        chatUIParent = ObjectManager.instance.chatUIParent;

        GetComponent<Button>().onClick.AddListener(OnClick);
        prefab_Chat = (GameObject)Resources.Load("Prefabs/MyChat");
    }

    private void OnClick()
    {
        // 채팅 오픈
        ChatManager.instance.OpenChatRoom();
        // 채팅 업데이트
        //curChatRoom 확인
    }


    public void UpdateUI_Chat()
    {
        // 대화 상대 표시
        if(chatPerson.Count != 0)
        {
            var s = chatPerson[0].Name;
            for(int i=1; i < chatPerson.Count; i++)
            {
                s += ", " + chatPerson[i].Name;
            }
            text_Person.text = s;
        }

        // todo: 내용이 너무 길때 처리하기
        // 대화 내용 표시
        text_Content.text = chatList[chatList.Count - 1].contents;
    }
}
