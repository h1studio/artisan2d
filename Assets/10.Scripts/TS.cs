﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TS {
    
    //public static void SetActive(GameObject g, bool b)
    //{
    //    g.SetActive(b);
    //}

    //public static void SetActive_Toggle(GameObject g)
    //{
    //    if (g.activeInHierarchy)
    //        g.SetActive(false);
    //    else
    //        g.SetActive(true);
    //}

    public static IEnumerator MoveObjectByTime(GameObject g, Vector3 pStartPosition, Vector3 pEndPosition, float pTime, UnityAction action)
    {
        var _event = new UnityEvent();
        if (action != null)
            _event.AddListener(action);

        if (pStartPosition == pEndPosition)
            yield break;

        float journeyLength = Vector3.Distance(pStartPosition, pEndPosition);
        float startTime = Time.time;        
        float distCovered = 0f;
        float fracJourney = 0f;
        float speed = journeyLength / pTime;

        while (fracJourney < 1f)
        {
            distCovered = (Time.time - startTime) * speed;
            fracJourney = distCovered / journeyLength;

            g.transform.position = Vector3.Lerp(pStartPosition, pEndPosition, fracJourney);
            yield return null;
        }

        if (_event != null)
            _event.Invoke();
    }
}
