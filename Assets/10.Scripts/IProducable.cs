﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IProducable {
    
    FrameDefinition frame { get; }
    OreDefinition ore { get; }

    ItemInfo ProduceItem(ItemDefinition item);
    bool CheckToProduce(FrameDefinition frame, OreDefinition ore);
}
