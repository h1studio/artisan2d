﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Int_Var
{   
    private int maxControllMethod = 0;
    

    /// <summary>
    /// pMaxController - 0: max 존재하지 않음, 1: max 넘을시 넣을 수 있는 양만 추가 후 나머지 리턴, 2: max 넘을시 추가 X, 3: Boundary 넘을시 반대 Boundary로 변경
    /// </summary>
    /// <param name="pValue"></param>
    /// <param name="pMaxValue"></param>
    /// <param name="pMaxControllMethod"></param>
    public Int_Var(int pValue, int pMaxValue = 100, int pMaxControllMethod = 0)
    {
        DefaultValue = pValue;
        Value = pValue;
        MaxValue = pMaxValue;
        maxControllMethod = pMaxControllMethod;
    }

    [SerializeField] private int _defaultValue = 0;
    public int DefaultValue { get { return _defaultValue; } set { _defaultValue = value; } }


    [SerializeField]
    private int _value;
    public int Value
    {
        get { return _value; }
        private set
        {
            _value = value;
            Notify_VarChanged();
        }
    }

    [SerializeField]
    private int _maxValue;
    public int MaxValue
    {
        get { return _maxValue; }
        set
        {
            _maxValue = value;
            Notify_VarChanged();
        }
    }

    public float ValueFactor
    {
        get { return Value / MaxValue; }
    }

    public void SetValue(int setBy)
    {
        if (setBy > MaxValue)
            Value = MaxValue;
        else
            Value = setBy;
    }
    public int ChangeValue(int changeBy)
    {
        if (Value > MaxValue)
            return 0;

        if (Value + changeBy > MaxValue)
        {
            switch (maxControllMethod)
            {
                case 0:         // max 존재하지 않음
                    Value += changeBy;
                    return 0;

                case 1:         // 넣을 수 있는 양만 추가 후 나머지 리턴
                    int returnValue = Value + changeBy - MaxValue;
                    Value = MaxValue;
                    return returnValue;

                case 2:         // 추가 X
                    return changeBy;

                case 3:
                    Value = 0;
                    return 0;                    

                default:
                    Debug.LogWarning("maxController 수치가 이상합니다. maxController : " + maxControllMethod);
                    break;
            }
        }
        else if(Value + changeBy < 0)
        {
            if (maxControllMethod == 3)
            {
                Value = MaxValue;
                return 0;
            }                
        }
        else
        {
            Value += changeBy;
        }
        return 0;
    }

    public void ResetValue()
    {
        Value = DefaultValue;
    }

    protected virtual void Start()
    {
        Notify_VarChanged();
    }

    public event Action<Int_Var> OnVarChanged;

    private void Notify_VarChanged()
    {
        if (OnVarChanged != null)
            OnVarChanged(this);
    }
}
