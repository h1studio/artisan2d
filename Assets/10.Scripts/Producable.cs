﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Producable : IProducable {

    public Producable(FrameDefinition pFrame, OreDefinition pOre)
    {
        _frame = pFrame;
        _ore = pOre;
    }
    public Producable(string pFrameName, string pOreName)
    {
        _frame = DefinitionList_Database.instance.GetFrameByName(pFrameName);
        _ore = DefinitionList_Database.instance.GetOreByName(pOreName);        
    }

    public bool CheckToProduce(FrameDefinition pFrame, OreDefinition pOre)
    {
        if (frame == pFrame && ore == pOre)
            return true;
        else
            return false;
    }

    public ItemInfo ProduceItem(ItemDefinition pItem)
    {
        return new ItemInfo(pItem);
    }

    [SerializeField]
    private FrameDefinition _frame;
    public FrameDefinition frame
    {
        get { return _frame; }
    }

    [SerializeField]
    private OreDefinition _ore;
    public OreDefinition ore
    {
        get { return _ore; }
    }
}
