﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Double_Var
{
    public event Action<Double_Var> OnVarChanged;

    //[Range(0,2)]
    private int maxControllMethod = 0;  // 0: max 존재하지 않음, 1: max 넘을시 넣을 수 있는 양만 추가 후 나머지 리턴, 2: max 넘을시 추가 X
    private double defaultValue = 0f;

    [SerializeField]
    private string varName;

    public Double_Var(string pName, double pValue)
    {
        varName = pName;
        defaultValue = pValue;
        value = pValue;
        maxControllMethod = 0;
    }
    /// <summary>
    /// pMaxController - 0: max 존재하지 않음, 1: max 넘을시 넣을 수 있는 양만 추가 후 나머지 리턴, 2: max 넘을시 추가 X
    /// </summary>
    /// <param name="pName"></param>
    /// <param name="pValue"></param>
    /// <param name="pMaxValue"></param>
    /// <param name="pMaxControllMethod"></param>
    public Double_Var(string pName, double pValue, double pMaxValue, int pMaxControllMethod)
    {
        varName = pName;
        defaultValue = pValue;
        value = pValue;
        maxValue = pMaxValue;
        maxControllMethod = pMaxControllMethod;
    }

    [SerializeField]
    private double _value = 0f;
    public double value
    {
        get { return _value; }

        private set
        {
            _value = value;
            Notify_VarChanged();
        }
    }

    [SerializeField]
    private double _maxValue = 100f;
    public double maxValue
    {
        get { return _maxValue; }
        set
        {
            _maxValue = value;
            Notify_VarChanged();
        }
    }

    public double valueFactor
    {
        get { return value / maxValue; }
    }

    public double ChangeValue(double changeBy)
    {
        if (value > maxValue)
            return 0f;

        if (value + changeBy >= maxValue)
        {
            switch (maxControllMethod)
            {
                case 0:         // max 존재하지 않음
                    value += changeBy;
                    return 0f;

                case 1:         // 넣을 수 있는 양만 추가 후 나머지 리턴
                    double returnValue = value + changeBy - maxValue;
                    value = maxValue;
                    return returnValue;

                case 2:         // 추가 X
                    return changeBy;

                default:
                    Debug.LogWarning("maxController 수치가 이상합니다. maxController : " + maxControllMethod);
                    break;
            }
        }
        else
        {
            value += changeBy;
        }
        return 0f;

    }

    public void ResetValue()
    {
        value = defaultValue;
    }

    protected virtual void Start()
    {
        Notify_VarChanged();
    }

    private void Notify_VarChanged()
    {
        if (OnVarChanged != null)
            OnVarChanged(this);
    }
}
