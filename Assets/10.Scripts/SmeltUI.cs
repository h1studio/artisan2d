﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SmeltUI : MonoBehaviour {

    [SerializeField]
    private Button btn_Smelt;

    [SerializeField]
    private GameObject moveToFrameObject;

    [SerializeField]
    private GraphicRaycaster ray;

    [SerializeField]
    private Furnace furnace;

    [SerializeField]
    private Text text_Ore;
    [SerializeField]
    private Text text_Temperature;
    [SerializeField]
    private Text text_SubTemperature;
    [SerializeField]
    private Text text_MeltingRatio;
    [SerializeField]
    private Text text_Mass;
    [SerializeField]
    private Text text_Purity;

    private void Start()
    {
        btn_Smelt.onClick.AddListener(OnClick_SmeltBtn);

        furnace.temperature.OnVarChanged += UpdateUI_Temperature;
        furnace.subTemperature.OnVarChanged += UpdateUI_SubTemperature;
        furnace.meltingRatio.OnVarChanged += UpdateUI_MeltingRatio;
        furnace.impureMass.OnVarChanged += UpdateUI_Mass;
        furnace.OnOreChanged += UpdateUI_Ore;

        //btn_PreviousMine.onClick.AddListener(delegate { MineManager.instance.MoveNextMine(-1); });
        //btn_NextMine.onClick.AddListener(delegate { MineManager.instance.MoveNextMine(1); });
    }

    private void OnClick_SmeltBtn()
    {
        SmeltManager.instance.CurFurnace.subTemperature.ChangeValue(200);
    }

    private bool isPointerMoving = false;
    public void OnPointerDown(BaseEventData pData)
    {
        PointerEventData data = pData as PointerEventData;
        if (SmeltManager.instance.CurFurnace.meltingRatio.Value >= 80)
        {
            moveToFrameObject.SetActive(true);
            moveToFrameObject.transform.position = data.position;
            isPointerMoving = true;
        }
    }

    public void OnDrag(BaseEventData pData)
    {
        if(isPointerMoving)
        {
            PointerEventData data = pData as PointerEventData;

            moveToFrameObject.transform.position = data.position;
            var frame = GetFrame(data);
        }
    }

    public void OnPointerUp(BaseEventData pData)
    {
        if(isPointerMoving)
        {
            PointerEventData data = pData as PointerEventData;

            moveToFrameObject.SetActive(false);
            var frame = GetFrame(data);
            if(frame != null)
                frame.AddOre();
        }
    }
    private Frame GetFrame(PointerEventData data)
    {
        //Frame이면 해당 프레임에 넣기 가능한지 확인
        Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        List<RaycastResult> results = new List<RaycastResult>();
        ray.Raycast(data, results);

        foreach (RaycastResult result in results)
        {
            if (result.gameObject.CompareTag("Frame"))
            {
                return result.gameObject.GetComponent<Frame>();
            }
        }
        return null;
    }

    private void UpdateUI_Temperature(Float_Var pValue)
    {
        text_Temperature.text = "용광로 온도 : " + (int)pValue.Value + " / " + pValue.MaxValue;
    }
    private void UpdateUI_SubTemperature(Float_Var pValue)
    {
        text_SubTemperature.text = "서브 온도 : " + (int)pValue.Value + " / " + pValue.MaxValue;
    }
    private void UpdateUI_MeltingRatio(Float_Var pValue)
    {
        text_MeltingRatio.text = "융해도 : " + (int)pValue.Value + " / " + pValue.MaxValue;
    }
    private void UpdateUI_Mass(Double_Var pValue)
    {
        text_Mass.text = "질량 : " + furnace.Mass + " / " + furnace.maxMass;
        text_Purity.text = "순도 : " + furnace.Purity * 100f + " %";
    }
    private void UpdateUI_Ore(OreDefinition pOre)
    {
        if (pOre != null)
            text_Ore.text = pOre.Name;
    }

    //[SerializeField]
    //private ButtonSetting btn_previousFurnace;
    //[SerializeField]
    //private ButtonSetting btn_nextFurnace;


    //private void OnEnable()
    //{
    //    btn_UpAmount.Delegate_OnClick += OnClick_UpAmount;
    //    btn_DownAmount.Delegate_OnClick += OnClick_DownAmount;
    //    btn_MaxAmount.Delegate_OnClick += OnClick_MaxAmount;

    //    //btn_previousFurnace.OnClickDele += OnClick_PreviousFurnace;
    //    //btn_nextFurnace.OnClickDele += OnClick_NextFurnace;
    //}

    //private void OnDisable()
    //{
    //    btn_UpAmount.Delegate_OnClick -= OnClick_UpAmount;
    //    btn_DownAmount.Delegate_OnClick -= OnClick_DownAmount;
    //    btn_MaxAmount.Delegate_OnClick -= OnClick_MaxAmount;

    //    //btn_previousFurnace.OnClickDele -= OnClick_PreviousFurnace;
    //    //btn_nextFurnace.OnClickDele -= OnClick_NextFurnace;
    //}

    //private void OnClick_UpAmount()
    //{
    //    SmeltManager.instance.inputOreAmount += 1;
    //    UpdateUI_AmountBtn();
    //}
    //private void OnClick_DownAmount()
    //{        
    //    SmeltManager.instance.inputOreAmount -= 1;
    //    UpdateUI_AmountBtn();
    //}
    //private void OnClick_MaxAmount()
    //{
    //    //SmeltManager.instance.inputMax = !SmeltManager.instance.inputMax;
    //}

    //private void OnClick_PreviousFurnace()
    //{
    //    SmeltManager.instance.curFurnaceIndex -= 1;
    //    UpdateUI_PNFurnaceBtn();
    //}
    //private void OnClick_NextFurnace()
    //{
    //    SmeltManager.instance.curFurnaceIndex += 1;
    //    UpdateUI_PNFurnaceBtn();
    //}

    //private void UpdateUI_AmountBtn()
    //{
    //    text_OreAmount.text = SmeltManager.instance.inputOreAmount.ToString();

    //    if (SmeltManager.instance.inputOreAmount == 1)
    //        btn_DownAmount.gameObject.SetActive(false);
    //    else
    //        btn_DownAmount.gameObject.SetActive(true);        
    //}
    //private void UpdateUI_PNFurnaceBtn()
    //{
    //    if (SmeltManager.instance.curFurnaceIndex == 0)
    //        btn_previousFurnace.gameObject.SetActive(false);
    //    else
    //        btn_previousFurnace.gameObject.SetActive(true);

    //    if (SmeltManager.instance.curFurnaceIndex == SmeltManager.instance.furnaces.Count - 1)
    //        btn_nextFurnace.gameObject.SetActive(false);
    //    else
    //        btn_nextFurnace.gameObject.SetActive(true);
    //}

    private void SetFurnace(Furnace pFurnace)
    {
        // 상태정보 바꾸기

        //UpdateUI_FurnaceInfo();
    }
}
