﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tesseract;

public class Button_Link : ButtonSetting
{
    [SerializeField]
    [Required]
    private Menu menu;
    
    protected override void OnClick_Button()
    {

        base.OnClick_Button();
        // 최근 버튼으로 설정
        menu.SetCurrentButton(this);

        // 패널 열기
        ActivePanel(true);
        // 버튼 색깔 변경
        SetButtonColor(changedColor);

    }
}
