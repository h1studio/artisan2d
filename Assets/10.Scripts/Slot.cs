﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour {

    [SerializeField]
    private Image slotImage;
    [SerializeField]
    private Text slotText;
    [SerializeField]
    private Slider slotSlider;
    
    private ISlotableObject _object;
    public ISlotableObject SlotObject
    {
        get { return _object; }
        private set { _object = value; }
    }

    [SerializeField]
    private bool isButtonPossible = true;
    public bool IsButtonPossible
    {
        get { return isButtonPossible; }
        set { isButtonPossible = value; }
    }
    //public bool isSetText = true;

    [SerializeField]
    private bool isSlider = false;
    public bool IsSlider
    {
        get { return isSlider; }
        set { isSlider = value; }
    }

    //public delegate void slotDelegate(Slot pSlot);
    //public slotDelegate Delegate_UpdateUI_Slot;

    public void SetSlot(ItemInfo pObject)
    {
        if(SlotObject != null && SlotObject != pObject)
        {
            RemoveSlot();
        }

        SlotObject = pObject;
        //item.AddSlot(this);
        Notify_SlotObjectChanged(SlotObject);
    }

    public void RemoveSlot()
    {
        //item.RemoveSlot(this);
        SlotObject = null;

        Notify_SlotObjectChanged(SlotObject);
    }
        
    private void Notify_SlotObjectChanged(ISlotableObject pItem)
    {
        //if (Delegate_UpdateUI_Slot != null)
        //    Delegate_UpdateUI_Slot(this);

        UpdateUI_Image();
        UpdateSlider();
    }

    private void UpdateUI_Image()
    {
        if (SlotObject != null)
            SetImage(SlotObject.Image);
        else
            RemoveImage();
    }
    private void SetImage(Sprite pSprite)
    {
        slotImage.sprite = pSprite;
    }
    private void RemoveImage()
    {
        slotImage.sprite = null;
    }

    private void UpdateSlider()
    {
        if (!IsSlider || SlotObject == null)
        {
            slotSlider.gameObject.SetActive(false);
            return;
        }

        if (SlotObject.IsHealthSlider)
        {
            slotSlider.gameObject.SetActive(true);
            //slotSlider.maxValue = Item.GetDurability();
            //slotSlider.value = Item.CurDurability;
        }
        else
        {
            slotSlider.gameObject.SetActive(false);
        }

    }

    //void OnDestroy()
    //{
    //    if (slotItem != null)
    //    {
    //        if (slotItem.slotList.Find(x => x == this) != null)
    //        {
    //            slotItem.slotList.Find(x => x == this).RemoveItem();
    //        }
    //    }
    //}
}

