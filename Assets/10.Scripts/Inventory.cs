﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class Inventory : MonoBehaviour {

    public event Action<ItemCollection> OnItemChanged;

    private ItemCollection equipItemCollection = new ItemCollection(20);
    private ItemCollection materialItemCollection = new ItemCollection(20);
    private ItemCollection etcItemCollection = new ItemCollection(20);

    private ISetItemFromInventory slot_Target;
    
    public void SetTarget(ISetItemFromInventory pTarget)
    {
        slot_Target = pTarget;
    }
    public void RemoveTarget()
    {
        slot_Target = null;
    }
    public bool SetItemToTarget(ItemInfo pItem)
    {
        if(slot_Target != null)
        {
            slot_Target.SetItem(pItem);
            return true;
        }
        return false;            
    }


    public ItemCollection GetCollection(ItemType pType)
    {
        switch (pType)
        {
            case ItemType.Equipment:
                return equipItemCollection;
            case ItemType.Material:
                return materialItemCollection;
            case ItemType.Etc:
                return etcItemCollection;
            default:
                break;
        }
        return null;
    }
    private ItemCollection GetCollection(ItemInfo pItem)
    {
        return GetCollection(pItem.Type);
    }

    public void AddItem(ItemInfo pItem)
    {
        Debug.Log(pItem.Name);
        var collection = GetCollection(pItem);
        collection.AddItem(pItem);

        Notify_ItemChanged(collection);
    }

    public void RemoveItem(ItemInfo pItem)
    {
        var collection = GetCollection(pItem);
        collection.RemoveItem(pItem);

        Notify_ItemChanged(collection);
    }

    public void Notify_ItemChanged(ItemCollection pCollection)
    {
        if (OnItemChanged != null)
            OnItemChanged(pCollection);
    }
}
