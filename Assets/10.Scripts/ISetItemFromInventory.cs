﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISetItemFromInventory
{

    void SetTarget();

    void SetItem(ItemInfo pItem);
}
