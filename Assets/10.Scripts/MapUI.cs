﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tesseract;

//todo : [RequireComponent] 세팅하기
//todo : [Required] 세팅하기
[RequireComponent(typeof(Button))]
public class MapUI : MonoBehaviour {
    
    [Required]
    [SerializeField]
    private MapDefinition mapDefinition;

    private MapInfo map;

    private Button btn;

    private void Awake()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(OnClick_Map);        
    }
    private void Start()
    {
        map = MapManager.instance.GetMapByDefinition(mapDefinition);
        map.mapUI = this;
    }

    public void OnClick_Map()
    {
        // 이동 모드
        if(Atlas.scheduleMode)
        {            
            if(Atlas.instance.IsMapInSchedule(map))     // 모험일정에 맵이 있음
            {
                Atlas.instance.RemoveSchedule(map);
                GetComponent<Image>().color = Color.white;

            }
            else        // 모험일정에 맵이 없음
            {
                // 모험 일정에 맵 추가
                if(Atlas.instance.SetSchedule(map))
                {
                    GetComponent<Image>().color = Color.yellow;
                }
            }
        }
        else
        {
            ObjectManager.instance.mapSelectUI.gameObject.SetActive(true);
            ObjectManager.instance.mapSelectUI.transform.position = gameObject.transform.position + Vector3.right * 100;
            ObjectManager.instance.mapSelectUI.SetMap(map);
        }

    }
}
