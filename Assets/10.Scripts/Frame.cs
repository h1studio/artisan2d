﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Frame : MonoBehaviour {

    [HideInInspector]
    public Double_Var pureMass = new Double_Var("PureMass", 0f);
    [HideInInspector]
    public Double_Var impureMass = new Double_Var("ImpureMass", 0f);

    public float maxMass = 10f;

    [SerializeField]
    private Text text_Mass;

    public double Mass
    {
        get { return pureMass.value + impureMass.value; }
    }
    public double Purity
    {
        get
        {
            if (Mass == 0)
                return 0f;
            else
                return pureMass.value / Mass;
        }
    }

    [SerializeField]
    private FrameDefinition frame;
    private OreDefinition curOre;

    public void AddOre()
    {
        Furnace curFurnace = SmeltManager.instance.CurFurnace;
        // 기존에 들어가있는 광석종류 확인
        if (curOre != null && curOre.ID != curFurnace.CurOre.ID)
        {
            Debug.Log("투입된 다른 종류의 광석이 있습니다.\n투입된 광석 : " + curOre.Name + ", 투입하려는 광석 : " + curFurnace.CurOre.Name);
            return;
        }

        // 틀에 얼마나 넣을지 확인
        double inputMass = 0f;
        if (maxMass - Mass <= curFurnace.Mass)
        {
            inputMass = maxMass - Mass;
        }
        else
            inputMass = curFurnace.Mass;

        // 틀에 넣기
        curOre = curFurnace.CurOre;
        double changePureMassValue = inputMass * curFurnace.Purity;
        double changeImpureMassValue = inputMass * (1 - curFurnace.Purity);
        curFurnace.pureMass.ChangeValue(-changePureMassValue);
        curFurnace.impureMass.ChangeValue(-changeImpureMassValue);
        pureMass.ChangeValue(changePureMassValue);
        impureMass.ChangeValue(changeImpureMassValue);

        // 틀 꽉참
        if (Mass == maxMass)
        {
            // 아이템 생성         
            ItemInfo tItem = ItemManager.instance.ProduceItem(frame, curOre);
            ObjectManager.instance.player.inventory.AddItem(tItem);

            // 틀 리셋
            ResetFrame();
        }
    }

    private void ResetFrame()
    {
        pureMass.ResetValue();
        impureMass.ResetValue();
        curOre = null;
    }

    //private void UpdateUI_Mass(Double_Var pStat)
    //{
    //    text_Mass.text = "질량 : " + frame.Mass + " / " + frame.maxMass;
    //    text_Mass.text += "\n순도 : " + frame.Purity * 100f + " %";
    //}
}
