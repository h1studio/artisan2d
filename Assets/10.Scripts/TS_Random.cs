﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TS_Random {

    /// <summary>
    /// pSuccessPercent의 성공확률을 가진 동전을 던집니다. (5% -> 5 입력)
    /// </summary>
    /// <param name="pSuccessPercent"></param>
    /// <returns></returns>
    static public bool GetBinaryRoll(float pSuccessPercent)
    {
        float value = Random.value * 100;

        if (value < pSuccessPercent)
            return true;
        else
            return false;
    }
}
