﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TS_Math {

    /// <summary>
    /// x를 y로 나누었을때의 몫을 반환합니다. 0으로 나눌경우 0을 반환합니다.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public static int Quotient(float x, float y)
    {
        if (y == 0)
        {
            Debug.LogError("0으로 나누었습니다. 오류");
            return 0;
        }
        return Mathf.FloorToInt(x / y);
    }
    public static int Quotient(int x, int y)
    {
        if (y == 0)
        {
            Debug.LogError("0으로 나누었습니다. 오류");
            return 0;
        }
        
        return Mathf.FloorToInt((float)x / (float)y);
    }

    /// <summary>
    /// x를 y로 나누었을때의 나머지를 반환합니다. 0으로 나눌경우 0을 반환합니다.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public static float Remainder(float x, float y)
    {
        if (y == 0)
        {
            Debug.LogError("0으로 나누었습니다. 오류");
            return 0;
        }
        return x - (y * Quotient(x,y));
    }
    public static int Remainder(int x, int y)
    {
        if (y == 0)
        {
            Debug.LogError("0으로 나누었습니다. 오류");
            return 0;
        }
        return x - (y * Quotient(x, y));
    }

    /// <summary>
    /// pNumber를 소숫점 아래 pIndex자리 까지만 표현합니다. 
    /// </summary>
    /// <param name="pIndex"></param>
    /// <returns></returns>
    public static float Round(float pNumber, int pIndex)
    {
        return Mathf.Round(pNumber * Mathf.Pow(10, pIndex)) / Mathf.Pow(10, pIndex);
    }
    public static float Floor(float pNumber, int pIndex)
    {
        return Mathf.Floor(pNumber * Mathf.Pow(10, pIndex)) / Mathf.Pow(10, pIndex);
    }


    /// <summary>
    /// 초 단위로 표현된 pTime의 분을 반환합니다. 예: 130초 -> 2
    /// </summary>
    /// <param name="pTime"></param>
    /// <returns></returns>
    public static float GetMin(float pTime)
    {
        return TS_Math.Quotient(pTime, 60);
    }

    /// <summary>
    /// 초 단위로 표현된 pTime의 초를 반환합니다. 예: 130초 -> 10
    /// </summary>
    /// <param name="pTime"></param>
    /// <returns></returns>
    public static float GetSec(float pTime)
    {
        return TS_Math.Round(TS_Math.Remainder(pTime, 60), 0);
    }


    public static int GetLevel(float pValue, int[] pBoundary)
    {
        for(int i=0; i < pBoundary.Length - 1; i++)
        {
            if(pBoundary[i] <= pValue && pValue <= pBoundary[i+1])
            {
                return i;
            }            
        }

        Debug.LogWarning("값이 해당 구간에 존재하지 않습니다. value : " + pValue );
        return -1;
    }
}
