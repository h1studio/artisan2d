﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopMenuUI : MonoBehaviour {
    
    [Header("UI")]
    [SerializeField]
    private TS_Text UI_Level;
    //[SerializeField]
    //private Slider slider_Exp;
    [SerializeField]
    private TS_Text UI_Money;
    [SerializeField]
    private TS_Text UI_Tesseract;
    [SerializeField]
    private TS_Text UI_Copper;
    [SerializeField]
    private TS_Text UI_Iron;
    [SerializeField]
    private TS_Text UI_Silver;
    [SerializeField]
    private TS_Text UI_Gold;

    public void Start()
    {
        UI_Level.SetLabel("레벨");
        UI_Money.SetLabel("돈");
        UI_Tesseract.SetLabel("테스");
        UI_Copper.SetLabel("구리");
        UI_Iron.SetLabel("철");
        UI_Silver.SetLabel("은");
        UI_Gold.SetLabel("금");

        ObjectManager.instance.player.OnOreChanged += UpdateUI_OreAmount;
    }

    public void UpdateUI_Player()
    {
        UI_Level.SetContent("");
        UI_Money.SetContent(ObjectManager.instance.player.money.Value);
        UI_Tesseract.SetContent(ObjectManager.instance.player.tesseract.Value);        
    }
    public void UpdateUI_OreAmount(OreCollection pOreCollection)
    {
        UI_Copper.SetContent(pOreCollection.GetOreAmount("구리 광석").ToString());
        UI_Iron.SetContent(pOreCollection.GetOreAmount("철 광석").ToString());
        UI_Silver.SetContent(pOreCollection.GetOreAmount("은 광석").ToString());
        UI_Gold.SetContent(pOreCollection.GetOreAmount("금 광석").ToString());
    }

}
