﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISlotableObject {

    Sprite Image { get; }
    bool IsHealthSlider { get; }
    string Name { get; }
}
