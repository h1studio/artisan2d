﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashWhite : MonoBehaviour {
    
        public float delay = 0.05f;
        public Material effectMaterial; // 인스펙터에서 PaintWhite든 GrayScale이던 추가합니다 ( 그레이스케일은 회색조로 표현재주지만 안알랴줌 키워드로 검색 ㄱㄱ )

        IEnumerator FlashCo()
        { // Co는 코루틴의 코! 
            SpriteRenderer render = this.GetComponent<SpriteRenderer>();
            Material originalMater = render.material;
            render.material = effectMaterial;

            yield return new WaitForSeconds(delay); // 하얀색으로 혹은 회색으로 바꾸거 일정 딜레이동안 보여줘야 사람들 눈에 반짝이는게 들어온다. 그래서 코루틴씀

            render.material = originalMater; // 색상표현 원상복구
        }

        public void Start()
        {            
            StartCoroutine(FlashCo());
        }
     // 간단 사용법 this.getComponent<FlashWhite>().FlashWhite();
}
