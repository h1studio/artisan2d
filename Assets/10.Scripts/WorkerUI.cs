﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkerUI : MonoBehaviour {

    private Worker curWorker;

    [SerializeField]
    private List<WorkerSlot> WorkerSlotList;

    [SerializeField]
    private Image image_WorkerFace;
    [SerializeField]
    private Text text_WorkerName;

    [SerializeField]
    private Button btn_AddWorker;
    [SerializeField]
    private Button btn_WorkWorker;

    [SerializeField]
    private Text text_mineName;

    [SerializeField]
    private Button btn_PreviousMine;
    [SerializeField]
    private Button btn_NextMine;

    //[SerializeField]
    //private CameraController cam;

    private void Awake()
    {
        btn_AddWorker.onClick.AddListener(OnClick_AddWorker);
        btn_WorkWorker.onClick.AddListener(OnClick_Work);

        WorkerManager.instance.OnWorkerListChanged += UpdateUI_WorkerList;
        MineManager.instance.OnMineChanged += UpdateUI_MineDescription;

        btn_PreviousMine.onClick.AddListener(delegate { MineManager.instance.MoveNextMine(-1); });
        btn_NextMine.onClick.AddListener(delegate { MineManager.instance.MoveNextMine(1); });        
    }

    public void OnClick_AddWorker()
    {
        WorkerManager.instance.AddWorker();
    }
    public void OnClick_Work()
    {
        curWorker.Work();

        //Notify_WorkerListChanged();
        //Notify_CurWorkerChanged();
    }
    public void SetCurWorker(Worker pWorker)
    {
        curWorker = pWorker;

        UpdateUI_WorkerDescription();
    }

    private void UpdateUI_WorkerList(List<Worker> pWorkerList)
    {
        for (int i = 0; i < WorkerSlotList.Count; i++)
        {
            if (i < pWorkerList.Count)
            {
                WorkerSlotList[i].SetWorker(pWorkerList[i]);
            }
            else
                WorkerSlotList[i].RemoveWorker();
        }
    }
    public void UpdateUI_WorkerDescription()
    {
        image_WorkerFace.sprite = curWorker.Image;
        text_WorkerName.text = curWorker.Name;
    }
    public void UpdateUI_MineDescription(Mine pMine)
    {
        text_mineName.text = pMine.stone.Level.ToString();
    }
}
