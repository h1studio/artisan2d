﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    public Stone stone;
    
    public Transform[] workerPosition;

    public Worker[] workerList;

    private void Start()
    {
        workerList = new Worker[workerPosition.Length];
    }

    public bool SetWorker(Worker pWorker)
    {
        if(CheckWorkerExist(pWorker))
        {
            Debug.Log("해당 일꾼이 이미 존재합니다.");
            return false;
        }

        for (int i = 0; i < workerList.Length; i++)
        {
            if (workerList[i] == null)
            {
                workerList[i] = pWorker;
                pWorker.Move(workerPosition[i]);
                return true;
            }
        }
        Debug.LogWarning("최대 투입수를 초과했습니다.");
        return false;
    }

    public void RemoveWorker(Worker pWorker)
    {
        for (int i = 0; i < workerList.Length; i++)
        {
            if (workerList[i] == pWorker)
            {
                workerList[i] = null;
                pWorker.Move(MineManager.instance.workerRestPosition);
                return;
            }
        }
    }

    public bool CheckWorkerExist(Worker pWorker)
    {
        foreach(Worker w in workerList)
        {
            if (w == pWorker)
                return true;
        }
        return false;
    }

    public void OnClick()
    {
        stone.Mining(ObjectManager.instance.player.statCollection);
    }
}
