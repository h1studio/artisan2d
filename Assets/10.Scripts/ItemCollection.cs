﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollection {
    
    public List<ItemInfo> ItemList = new List<ItemInfo>();
    private int collectionSize = 1;
    public int Count { get { return ItemList.Count; } }

    public ItemCollection(int pMaxSize = 9999)
    {
        Resize(pMaxSize);
    }
    
    private bool CanAdd
    {
        get
        {
            if (ItemList.Count < collectionSize)
                return true;
            Debug.Log("여유 공간이 없습니다.");
            return false;
        }
    }

    public void AddItem(ItemInfo pItem)
    {
        if(CanAdd)
        {
            ItemList.Add(pItem);
        }        
    }

    public void RemoveItem(ItemInfo pItem)
    {
        foreach(ItemInfo item in ItemList)
        {
            if(item == pItem)
            {
                ItemList.Remove(item);
                return;
            }
        }
    }

    public void Resize(int pSize)
    {
        collectionSize = pSize;
    }

    public void RemoveAll()
    {
        ItemList = new List<ItemInfo>();
    }
}
