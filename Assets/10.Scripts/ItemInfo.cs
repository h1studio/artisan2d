﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemInfo : ISlotableObject
{
    public Status status;
    [SerializeField]
    private ItemDefinition itemDefinition;
    public int amount = 1;

    private List<Slot> slotList = new List<Slot>();

    public ItemInfo(ItemDefinition pItem)
    {
        itemDefinition = pItem;
        
        // 초기세팅
        if(pItem.canFight)
            status = new Status(pItem.statusGenerator.GenerateStatus());
    }
    public ItemInfo(string pItemName, Status pStatus = null)
    {
        ItemDefinition tItem =  DefinitionList_Database.instance.GetItemByName(pItemName);
        if (tItem != null)
        {
            itemDefinition = tItem;

            // 초기세팅
            if (pStatus != null)
            {
                status = new Status(pStatus);
                return;
            }
            else if (tItem.canFight)
            {
                status = new Status(tItem.statusGenerator.GenerateStatus());
                return;
            }
        }
        else
            Debug.LogWarning("해당 아이템이 없습니다. 입력 아이템 : " + pItemName);
    }

    public Sprite Image { get { return itemDefinition.Icon; } }
    public ItemType Type { get { return itemDefinition.ItemType; } }
    public ItemType_Refined RefinedType { get { return itemDefinition.ItemType_Refined; } }
    public string Name { get { return itemDefinition.Name; } }
    public uint ID { get { return itemDefinition.ID; } }
    public List<AffixDefinition> PossibleAffixList { get { return itemDefinition.affixList; } }
    public bool IsHealthSlider { get { return Type == ItemType.Equipment; } }

    public bool isEquip;

    // 0:장식, 1:세공, 2:각인
    private AffixDefinition[] affixList = new AffixDefinition[3];

    public void SetAffix(AffixDefinition pAffix)
    {
        if(affixList[(int)pAffix.affixType] == null)
        {
            affixList[(int)pAffix.affixType] = pAffix;
            status.AddStatus(pAffix.status);
        }
    }
    public AffixDefinition GetAffix(int pIndex)
    {
        return affixList[pIndex];
    }

    public void RemoveAffix(AffixDefinition pAffix)
    {
        affixList[(int)pAffix.affixType] = null;
        status.RemoveStatus(pAffix.status);
    }

    //public ItemInfo(int pItemID)
    //{
    //    foreach (ItemDefinition tItem in ItemListDatabase.instance.itemList)
    //    {
    //        if (tItem.ID == pItemID)
    //        {
    //            itemDefinition = tItem;
    //            return;
    //        }
    //    }
    //}


    //public void AddSlot(Slot pSlot)
    //{
    //    slotList.Add(pSlot);
    //}
    //public void RemoveSlot(Slot pSlot)
    //{
    //    slotList.Remove(pSlot);
    //}
    //public void DestroyItem()
    //{
    //    for(int i = slotList.Count - 1; i >= 0; i--)
    //    {
    //        slotList[i].RemoveItem();
    //    }
    //}
}
