﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum ItemType
{
    Equipment = 0,
    Material = 1,
    Etc = 2
}


public enum ItemType_Refined
{
    Material_Decorate = 0,
    Material_Craft = 1,
    Equip_Weapon = 2,
    Equip_Glove = 3,
    Equip_Shoes = 4,
    Equip_Helmet = 5,
    Equip_Leggings = 6,
    Equip_Armour = 7,


    Etc = 10
}

//ItemRefinedType 0,1은 AffixType 0,1과 연결되있음. 숫자변경 금지
public enum AffixType
{    
    Decorate = 0,
    Craft = 1,
    Engrave = 2
}


public enum AttackType
{
    Fire = 0,
    Water = 1,
    Wind = 2,
    Earth = 3
}

public enum FatherState
{
    Rest = 0,
    Move = 1,
    Adventure = 2,
    Return = 3   

}
public enum CustomerState
{
    Rest = 0,
    Enter = 1,
    Order = 2,
    Return = 3
}
public enum OrderType
{
    General = 0,
    Goblin = 1,
    Troll = 2,
    Etc = 3
}



public class EnumDatabase {
    public static T GetEnumValue<T>(string str) where T : struct, IConvertible
    {
        if (!typeof(T).IsEnum)
        {
            throw new Exception("T must be an Enumeration type.");
        }
        T val = ((T[])Enum.GetValues(typeof(T)))[0];
        if (!string.IsNullOrEmpty(str))
        {
            foreach (T enumValue in (T[])Enum.GetValues(typeof(T)))
            {
                if (enumValue.ToString().ToUpper().Equals(str.ToUpper()))
                {
                    val = enumValue;
                    break;
                }
            }
        }

        return val;
    }

    public static ItemType GetItemType(ItemType_Refined pType)
    {
        if (pType == ItemType_Refined.Material_Craft ||
            pType == ItemType_Refined.Material_Decorate)
        {
            return ItemType.Material;
        }
        else if (pType == ItemType_Refined.Etc)
            return ItemType.Etc;
        else
            return ItemType.Equipment;
    }
}

