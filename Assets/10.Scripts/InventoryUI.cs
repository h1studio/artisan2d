﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour {

    public GameObject slotParent;

    public Inventory inventory;

    [HideInInspector]
    public Slot[] slotList;

    [SerializeField]
    private int rowCount;
    [SerializeField]
    private int columnCount;
    [SerializeField]
    private float slotSize;
    [SerializeField]
    private float slotGap;

    [SerializeField]
    private Button btn_Equip;
    [SerializeField]
    private Button btn_Material;
    [SerializeField]
    private Button btn_Etc;

    [Header("Description")]
    //[SerializeField]
    //private Slot slot_Item;
    [SerializeField]
    private Text text_ItemName;

    private ItemInfo descriptionItem;

    private ItemCollection curItemCollection;
    
    private void Awake()
    {
        inventory.OnItemChanged += UpdateUI_InventoryItem;

        RemoveSlots();
        CreateInventorySlots();

        //SetClickFunctionToSlot();

        btn_Equip.onClick.AddListener( delegate { SetCollection(ItemType.Equipment); });
        btn_Material.onClick.AddListener(delegate { SetCollection(ItemType.Material); });
        btn_Etc.onClick.AddListener(delegate { SetCollection(ItemType.Etc); });

        SetCollection(ItemType.Equipment);
    }


    // 0 : 장비, 1 : 재료, 2 : 기타
    public void OpenInventory(int pIndex)
    {
        OpenInventory((ItemType)pIndex);
    }
    public void OpenInventory(ItemType pItemType)
    {
        gameObject.SetActive(true);
        SetCollection(pItemType);
    }

    private void SetCollection(ItemType itemType)
    {
        var collection = inventory.GetCollection(itemType);
        curItemCollection = collection;

        UpdateUI_InventoryItem(collection);
    }
    public void CloseInventory()
    {
        inventory.RemoveTarget();
        RemoveDescription();
        SetColorDefault();
        gameObject.SetActive(false);
    }

    //private void SetClickFunctionToSlot()
    //{
    //    Debug.Log(slotList.Length);

    //    for (int i = 0; i < slotList.Length; i++)
    //    {
    //        Debug.Log(slotList[i]);
    //        slotList[i].GetComponent<Button>().onClick.AddListener(delegate { AAA(i); });
    //        Debug.Log(i);
    //    }
    //}

    private void SetDescription(ItemInfo pItem)
    {
        text_ItemName.text = pItem.Name;
        descriptionItem = pItem;
    }
    private void RemoveDescription()
    {
        text_ItemName.text = "";
        descriptionItem = null;
    }

    public void OnClick_Slot(Slot pSlot)
    {
        if (pSlot.SlotObject == null)
        {
            Debug.Log("슬롯에 아이템이 없습니다.");
            return;
        }

        Debug.Log("클릭한 슬롯 아이템 : " + pSlot.SlotObject.Name);
                
        if(descriptionItem != pSlot.SlotObject)
        {
            SetDescription((ItemInfo)pSlot.SlotObject);
        }
        else
        {
            if(pSlot.IsButtonPossible)
            {
                if(inventory.SetItemToTarget((ItemInfo)pSlot.SlotObject))
                    CloseInventory();
            }
        }
    }

    public void UpdateUI_InventoryItem(ItemCollection pCollection)
    {
        if (pCollection != curItemCollection)
            return;

        for(int i=0; i < slotList.Length; i++)
        {
            if (i < pCollection.ItemList.Count)
                slotList[i].SetSlot(pCollection.ItemList[i]);
            else
                slotList[i].RemoveSlot();
        }
    }

    private void RemoveSlots()
    {
        for(int i=0; i < slotList.Length; i++)
        {
            Destroy(slotList[i].gameObject);
        }
        slotList = null;
    }

    [ContextMenu("Create Inven")]
    public void CreateInventorySlots()
    {
        GameObject slotPrefab = (GameObject)Resources.Load("Prefabs/Slot");
        slotPrefab.GetComponent<RectTransform>().sizeDelta = new Vector2(slotSize, slotSize);
        slotList = new Slot[rowCount * columnCount];

        for (int x = 0; x < rowCount; x++)
        {
            for (int y = 0; y < columnCount; y++)
            {
                GameObject slot = (GameObject)Instantiate(slotPrefab);
                slot.transform.SetParent(slotParent.transform);
                slot.transform.localScale = Vector3.one;
                RectTransform slotRect = slot.GetComponent<RectTransform>();

                slot.name = "slot_" + x + "_" + y;

                slotList[columnCount * x + y] = slot.GetComponent<Slot>();

                slotRect.localPosition = new Vector3((slotSize * y) + (slotGap * (y + 1)),
                                                   -((slotSize * x) + (slotGap * (x + 1))),
                                                      0);

                slot.GetComponent<Button>().onClick.AddListener(delegate { OnClick_Slot(slot.GetComponent<Slot>()); });

                //slot.GetComponent<ButtonSetting>().playSound = true;
                //slot.GetComponent<ButtonSetting>().buttonSoundClip = SoundManager.GetInstance().buttonSound_Default;
                //slot.GetComponent<Slot>().IsSlider = true;
            }
        }
    }

    public void ClassifyItem(ItemType pItemType)
    {
        for (int i = 0; i < slotList.Length; i++)
        {
            if (slotList[i].SlotObject != null)
            {
                if (!(slotList[i].SlotObject is ItemInfo))
                    continue;

                if (CheckItemType(pItemType, ((ItemInfo)slotList[i].SlotObject).Type))
                {
                    SetEnabled(slotList[i]);
                }
                else
                {
                    SetDisabled(slotList[i]);
                }
            }
            else
            {
                SetDisabled(slotList[i]);
            }
        }
    }
    public void ClassifyItem(ItemType_Refined pItemType)
    {
        for (int i = 0; i < slotList.Length; i++)
        {
            if (slotList[i].SlotObject != null)
            {
                if (!(slotList[i].SlotObject is ItemInfo))
                    continue;

                if (CheckItemType(pItemType, ((ItemInfo)slotList[i].SlotObject).RefinedType))
                {
                    SetEnabled(slotList[i]);
                }
                else
                {
                    SetDisabled(slotList[i]);
                }
            }
            else
            {
                SetDisabled(slotList[i]);
            }
        }
    }
    public void ClassifyItem(ItemInfo pItem)
    {
        for (int i = 0; i < slotList.Length; i++)
        {
            if (!(slotList[i].SlotObject is ItemInfo))
                continue;

            if (slotList[i].SlotObject != null)
            {
                if (CheckItemValue(pItem, (ItemInfo)slotList[i].SlotObject))
                {
                    SetEnabled(slotList[i]);
                }
                else
                {
                    SetDisabled(slotList[i]);
                }
            }
            else
            {
                SetDisabled(slotList[i]);
            }
        }
    }


    private void SetDisabled(Slot pSlot)
    {
        pSlot.GetComponent<Image>().color = Color.black;
        //pSlot.GetBackgroundImage().color = new Color(0.5f, 0.5f, 0.5f);
        //pSlot.GetImage().color = new Color(0.35f, 0.35f, 0.35f);
        pSlot.IsButtonPossible = false;
    }
    private void SetEnabled(Slot pSlot)
    {
        pSlot.GetComponent<Image>().color = Color.white;
        //pSlot.GetBackgroundImage().color = new Color(1f, 1f, 1f);
        //pSlot.GetImage().color = new Color(1f, 1f, 1f);
        pSlot.IsButtonPossible = true;
    }
    private void SetColorDefault()
    {
        for (int i = 0; i < slotList.Length; i++)
        {
            SetEnabled(slotList[i]);
        }
    }

    private bool CheckItemType(ItemType_Refined pCriteria, ItemType_Refined pTest)
    {
        if (pCriteria == pTest)
        {
            return true;
        }
        return false;
    }
    private bool CheckItemType(ItemType pCriteria, ItemType pTest)
    {
        if (pCriteria == pTest)
        {
            return true;
        }
        return false;
    }
    private bool CheckItemValue(ItemInfo pCriteria, ItemInfo pTest)
    {
        if (pCriteria.ID == pTest.ID //&&
            //pCriteria.GetItemPurity() <= pTest.GetItemPurity() &&
            //pCriteria.GetDefaultDurability() <= pTest.GetDurability()
            )
        {
            if (pTest.isEquip)
            { return false; }

            //if ((pCriteria.FirstAffix != null && pTest.FirstAffix == null) || (pCriteria.FirstAffix != null && pCriteria.FirstAffix.affixName != pTest.FirstAffix.affixName))
            //{
            //    return false;
            //}

            //if ((pCriteria.SecondAffix != null && pTest.SecondAffix == null) || (pCriteria.SecondAffix != null && pCriteria.SecondAffix.Tier > pTest.SecondAffix.Tier))
            //{
            //    return false;
            //}
            //if ((pCriteria.ThirdAffix != null && pTest.ThirdAffix == null) || (pCriteria.ThirdAffix != null && pCriteria.ThirdAffix.Tier > pTest.ThirdAffix.Tier))
            //{ return false; }

            return true;
        }
        return false;
    }
}
