﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections;
using Tesseract;

public class PiledOre : MonoBehaviour
{
    public static Coroutine inputCoroutine;
    public static int inputOreAmount = 1;
    
    public OreDefinition ore;

    [SerializeField]
    private float inputDelay_firstInput = 0.1f;
    [SerializeField]
    private float inputDelay = 0.1f;
    
    //public static int pointerID;
    
    [SerializeField]
    private Text text_OreAmount;

    //private void Awake()
    //{
    //    ChangeCartOreColor();
    //}
    private void Start()
    {
        ObjectManager.instance.player.OnOreChanged += UpdateUI_OreAmount;
    }


    public void InputBuzzerOn()
    {
        inputCoroutine = StartCoroutine(InputCoroutine());
    }

    public void InputBuzzerOff()
    {
        if (inputCoroutine != null)
        {
            StopCoroutine(inputCoroutine);
            inputCoroutine = null;
        }
    }

    private IEnumerator InputCoroutine()
    {
        PutInFurnace();
        yield return new WaitForSeconds(inputDelay_firstInput);
        while (true)
        {
            PutInFurnace();
            yield return new WaitForSeconds(inputDelay);
        }
    }

    private void PutInFurnace()
    {
        // 플레이어가 보유중인지 확인
        if (ObjectManager.instance.player.GetOreAmount(ore) < inputOreAmount)
        {
            Debug.Log("투입하려는 광석이 부족합니다.\n투입 개수 : " + inputOreAmount + ", 보유 개수 : " + ObjectManager.instance.player.GetOreAmount(ore));
            return;
        }

        OreInfo tOreInfo = new OreInfo(ore, inputOreAmount);

        // 용광로에 투입 가능한지 확인
        if (!SmeltManager.instance.CurFurnace.CheckToAddOre(tOreInfo))
        {
            return;
        }

        // 용광로에 투입
        ObjectManager.instance.player.ReduceOre(tOreInfo);      // 플레이어 보유수량 감소
        SmeltManager.instance.CurFurnace.AddOre(tOreInfo);      // 용광로에 투입        
    }
    
    #region UI Functions
    public void OnPointerDown(BaseEventData pData)
    {
        PointerEventData pointerData = pData as PointerEventData;
        
        if (inputCoroutine != null)
            return;
        
        //pointerID = pointerData.pointerId;
        InputBuzzerOn();
    }
    public void OnPointerUp(BaseEventData pData)
    {
        PointerEventData pointerData = pData as PointerEventData;

        //if (pointerID == pointerData.pointerId)
        //{
        //    pointerID = 0;
        InputBuzzerOff();
        //}
    }

    private void UpdateUI_OreAmount(OreCollection pOreCollection)
    {
        text_OreAmount.text = pOreCollection.GetOreAmount(ore).ToString();
    }
    private void ChangeCartOreColor()
    {
        GetComponent<Image>().color = ore.oreColor;
    }
    #endregion
}
