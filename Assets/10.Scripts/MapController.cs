﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : ManagerBase<MapController> {

    private MapInfo curMap;                                         // 현재 이야기중인 맵

    private StatCollection stat;
    private Status newStatus;

    public int storyCount_Default;
    private Int_Var storyCount;

    public float rarity_Default;
    private Float_Var rarity;

    public int enemyDropItemAmount_Default;
    private Int_Var enemyDropItemAmount;
    public int bossExtraDropItemAmount;

    public int fightNum_Default;
    private Int_Var fightNum;
    public int bossExtraFightNum;

    private ItemCollection itemCollection;

    [SerializeField]
    private float introWait;
    [SerializeField]
    private float storyWait;
    [SerializeField]
    private float roundWait;
    [SerializeField]
    private float fightWait;
    [SerializeField]
    private float fightPointWait;

    [SerializeField]
    private int mapNameTextSize;

    private ChatRoom chatRoom;
    private IChattableObject father;
    private IChattableObject system;

    private Chat tChat;

    private bool isFightFinished;

    private void Start()
    {
        father = ObjectManager.instance.systemChat;
        system = ObjectManager.instance.systemChat;

        storyCount = new Int_Var(storyCount_Default);
        rarity = new Float_Var(rarity_Default);
        enemyDropItemAmount = new Int_Var(enemyDropItemAmount_Default);
        fightNum = new Int_Var(fightNum_Default);

        itemCollection = new ItemCollection();
    }

    public void StoryStart(MapInfo pMap)
    {
        Debug.Log(pMap.Name + " 모험 시작");
        chatRoom = ChatManager.instance.MakeAdventureChatRoom();

        StartCoroutine(StoryCoroutine(pMap));
    }

    private IEnumerator StoryCoroutine(MapInfo pMap)
    {
        curMap = pMap;

        // 스토리 관련 변수들 초기화(데미지, 내구도, 아이템확률 등)
        SetDefaultValue();

        // 맵 이름
        Chat(system, curMap.Name, null, mapNameTextSize);
        Debug.Log("CheckPoint");
        yield return new WaitForSeconds(introWait);
        Chat(system, curMap.IntroText, new Color(1.0f, 0.64f, 0.0f));     // 오렌지색

        if (curMap.AdventureType == AdventureType.None)
        {
            //StopAllCoroutines();
        }
        else if (curMap.AdventureType == AdventureType.General)
        {
            //  storyCount 수 만큼 스토리 실행, 스토리간 사이는 여백
            while (storyCount.Value > 0)
            {
                storyCount.ChangeValue(-1);
                yield return new WaitForSeconds(storyWait);

                //todo : 확률 적용
                bool isFight = TS_Random.GetBinaryRoll(50);
                if(isFight)
                    yield return StartCoroutine(Fight());
                else
                    yield return StartCoroutine(ActiveStory());
            }
        }
        else if (curMap.AdventureType == AdventureType.Boss)
        {
            fightNum.ChangeValue(bossExtraFightNum);
            enemyDropItemAmount.ChangeValue(bossExtraDropItemAmount);
            yield return StartCoroutine(Fight());
        }

        // 스토리 종료. 아이템 정리 및 획득, 내구도 감소 등
        yield return new WaitForSeconds(storyWait);
        StoryEnd();
    }

    private void SetDefaultValue()
    {
        storyCount.ResetValue();
        rarity.ResetValue();
        enemyDropItemAmount.ResetValue();
        fightNum.ResetValue();

        itemCollection.RemoveAll();
        //itemList = new List<Item>();

        //playerDamageMultiplier = 1f;
        //playerDefenceMultiplier = 1f;
        //fightFrequencyMultiplier = 1f;
        //enemyDamageMultiplier = 1f;

        //roundFinished = false;

        //rewardBtn1.gameObject.SetActive(false);
        //rewardBtn2.gameObject.SetActive(false);
        //runawayBtn.gameObject.SetActive(true);
        //buyMusicBtn.gameObject.SetActive(false);
        //notBuyMusicBtn.gameObject.SetActive(false);

        //isSuccess = true;
    }

    private IEnumerator ActiveStory()
    {
        //todo: roll story
        StoryDefinition story = new StoryDefinition();

        Chat(father, story.Content);

        EffectStory(story);
        yield return null;
    }

    private IEnumerator Fight()
    {
        EnemyInfo enemy = new EnemyInfo(curMap.Enemy);
        Debug.Log("Enemy Name : " + enemy.Name);
        //Debug.Log("Enemy Health : " + enemy.statCollection.Health);

        yield return new WaitForSeconds(fightWait);

        //todo: 사운드조절
        //SoundManager.GetInstance().PlaySound_FightStart();

        Chat(father, enemy.Name + "'이(가) 나타났다!", Color.yellow);

        for (int i = 0; i < fightNum.Value; i++)
        {
            isFightFinished = false;

            yield return new WaitForSeconds(roundWait);

            yield return StartCoroutine(HitPlayer(enemy));

            yield return StartCoroutine(HitEnemy(enemy));

            if (isFightFinished)
                break;
        }

        Chat(father, "무승부");
        Debug.Log("무승부");
    }
    IEnumerator HitPlayer(EnemyInfo pEnemy)
    {
        yield return new WaitForSeconds(fightWait);

        Chat(father, pEnemy.Name + "'이(가) 용사를 공격, ");

        var damage = DamageCalculator.Attack(pEnemy.statCollection, stat);

        //float actualDamage;
        //actualDamage = CalculateDamage(pEnemy.damage * enemyDamageMultiplier);
        //actualDamage -= VillaManager.GetInstance().Defence * playerDefenceMultiplier;
        //actualDamage = TS_Math.Round(actualDamage, 0);

        //if (actualDamage <= 1)
        //    actualDamage = 1; // 아무리 방어력이 높아도 최소 1 피해받음.

        //todo: 아이템 내구도 감소
        //VillaManager.GetInstance().SubtractDurability(actualDamage);

        yield return new WaitForSeconds(fightPointWait);

        Chat(father, damage + " 피해!");

        yield return new WaitForSeconds(fightWait);

        //todo : 내구도 표현
        //Chat(father, "\n방어구 내구도가 " + TS_Math.Round(VillaManager.GetInstance().curDurability, 0) + "만큼 남았습니다", Color.green);

        if(stat.IsDead)
        {
            //todo: 사운드
            //SoundManager.GetInstance().PlaySound_FightLose();

            Chat(father, "장비들이 다 부서져서 어쩔 수 없이 도망갔지. 튼튼하게 만들어주지 않으련?", Color.red);

            Lose();
        }
    }
    IEnumerator HitEnemy(EnemyInfo pEnemy)
    {
        //float actualDamage;
        //actualDamage = CalculateDamage(VillaManager.GetInstance().Damage * playerDamageMultiplier);
        //actualDamage = TS_Math.Round(actualDamage, 0);

        var damage = DamageCalculator.Attack(stat, pEnemy.statCollection);

        if (damage <= 0)
            Chat(father, "무기가 없어서 주먹질을 해봤지만 아무런 효과가 없었지.");
        else
        {
            Chat(father, "\n용사가 '" + pEnemy.Name + "'을(를) 공격, ");

            //todo:아이템 내구도 감소
            //VillaManager.GetInstance().SubtractWeaponDurability(TS_Math.Round(pEnemy.defence, 0));

            yield return new WaitForSeconds(fightPointWait);
            Chat(father, damage + " 피해!", Color.red);
            //Chat(father, "\n무기 내구도가 " + TS_Math.Round(VillaManager.GetInstance().weaponCurDurability, 0) + "만큼 남았습니다", Color.green);
        }

        if(pEnemy.statCollection.IsDead)
        {
            yield return new WaitForSeconds(fightWait);
            
            Chat(father, pEnemy.Name + "'이(가) 치명상을 입었습니다.");

            //todo:사운드
            //SoundManager.GetInstance().PlaySound_FightWin();

            Chat(father, "아들이 만들어준 튼튼한 장비덕에 가볍게 이겼구나.", Color.blue);

            yield return new WaitForSeconds(fightWait);

            //todo: 아이템 획득
            //int itemCount = 0;
            //for (int i = 0; i < enemyDropItemAmount; i++)
            //{
            //    Debug.Log("text : " + i);
            //    if (TS_Random.GetBinaryRoll(rarity))
            //    {
            //        Item dropItem = pEnemy.GetDropItem();
            //        itemCount += 1;
            //        AddItem(dropItem);
            //        SetText("\n" + dropItem.GetName(), "Red");
            //        SetText("을 획득하였습니다.");
            //        SoundManager.GetInstance().PlaySound_AcquireItem();
            //        yield return new WaitForSeconds(0.2f);
            //    }
            //}
            //if (itemCount == 0)
            //    SetText("\n" + pEnemy.enemyName + "이(가) 가진 게 하나도 없어서 망연자실했었지.");
            isFightFinished = true;
        }
    }
    //float CalculateDamage(float pDamage)
    //{
    //    return TS_Random.Range((int)(pDamage * 0.7), (int)(pDamage * 1.2));
    //}

    private void Lose()
    {
        StopAllCoroutines();
    }

    void StoryEnd()
    {
        switch (itemCollection.Count)
        {
            case 0:
                Chat(father, "\n얻은 건 경험뿐이었구나.");
                break;
            case 1:
                Chat(father, "\n별로 건진 게 없는 여정이었지.");
                break;
            case 2:
                Chat(father, "\n돌이켜보면, 그 정도라도 건진 게 어디였나 싶더구나.");
                break;
            case 3:
                Chat(father, "\n꽤 짭짤한 모험이었지. 크흠");
                break;
            case 4:
                Chat(father, "\n아이템을 많이 얻었구나. 이 맛에 용사하는거 아니겠니. 하하핫");
                break;
            case 5:
                Chat(father, "\n한 마디로 대.박.모.험 이었다고 얘기할 수 있겠구나.");
                break;
            case 6:
                Chat(father, "\n다 가져오기 어려울 정도로 얻은 게 많은 모험이었구나.");
                break;
            default:
                Chat(father, "\n다 가져오기 어려울 정도로 얻은 게 많은 모험이었구나.");
                break;
        }

        //SetText("\n총 획득한 아이템");
        //foreach (Item item in itemList)
        //{
        //    SetText("\n" + item.GetName(), "Red");
        //}
        //if(itemList.Count !=0)
        //    SetText("\n" + "이 아이템들을 내가 다 쓸 필요는 없는데... \n\n어떠냐. 아빠의 이야기는 들을 만했느냐?");

        //rewardBtn1.gameObject.SetActive(true);
        //rewardBtn2.gameObject.SetActive(true);
        //runawayBtn.gameObject.SetActive(false);
        //buyMusicBtn.gameObject.SetActive(false);
        //notBuyMusicBtn.gameObject.SetActive(false);
    }
    void EffectStory(StoryDefinition pStory)
    {
        // index에 해당하는 아이템 획득
        if (pStory.EffectName == "Item")
        {
            ItemInfo item = new ItemInfo(DefinitionList_Database.instance.GetItemByID((int)pStory.EffectValue));
            if (item != null)
            {
                //AddItem(item);
                Chat(system, item.Name + "획득",Color.red);
            }
        }
        // 데미지 %증감 (ex. 20 -> 데미지 20% 증가)
        if (pStory.EffectName == "Damage")
        {
            //playerDamageMultiplier *= (pStory.EffectValue / 100) + 1;
            if (pStory.EffectValue > 0)                
                Chat(system," (데미지 " + pStory.EffectValue + "% 일시적 증가)", Color.green);
            else
                Chat(system, " (데미지 " + (-pStory.EffectValue) + "% 일시적 감소)", Color.green);
        }
        // 방어력 % 증감 (ex. 20 -> 방어력 20 % 증가)
        if (pStory.EffectName == "Defence")
        {
            //playerDefenceMultiplier *= (pStory.EffectValue / 100) + 1;
            if (pStory.EffectValue > 0)
                Chat(system, " (방어력 " + (pStory.EffectValue) + "% 일시적 증가)", Color.green);
            else
                Chat(system, " (방어력 " + (-pStory.EffectValue) + "% 일시적 감소)", Color.green);
        }
        // 내구도 증감 (ex. -20 -> 현재 내구도의 20 % 감소)
        if (pStory.EffectName == "Durability")
        {
            //VillaManager.GetInstance().SubtractDurability(VillaManager.GetInstance().curDurability * -(pStory.storyAttributes[i].attributeValue / 100));

            if (pStory.EffectValue > 0)
                Chat(system, " (내구도 " + (pStory.EffectValue) + "% 증가)", Color.green);
            else
                Chat(system, " (내구도 " + (-pStory.EffectValue) + "% 영구 감소)", Color.green);
        }
        // 아이템 획득 확률 % 증감 (ex. 20 -> 아이템 획득 확률 20% 증가)
        if (pStory.EffectName == "Rarity")
        {
            //rarity *= (pStory.EffectValue / 100) + 1;
            if (pStory.EffectValue > 0)
                Chat(system, " (아이템 획득 확률 " + (pStory.EffectValue) + "% 일시적 증가)", Color.green);
            else
                Chat(system, " (아이템 획득 확률 " + (-pStory.EffectValue) + "% 일시적 감소)", Color.green);
        }
        // 몬스터 공격력 % 증감
        if (pStory.EffectName == "MonsterDamage")
        {
            //enemyDamageMultiplier *= (pStory.EffectValue / 100) + 1;
            if (pStory.EffectValue > 0)
                Chat(system, " (몬스터 공격력 " + (pStory.EffectValue) + "% 증가)", Color.green);
            else
                Chat(system, " (몬스터 공격력 " + (-pStory.EffectValue) + "% 감소)", Color.green);
        }
        // 이야기 횟수 증감
        if (pStory.EffectName == "Count")
        {
            storyCount.ChangeValue((int)pStory.EffectValue);
            if (pStory.EffectValue > 0)
                Chat(system, " (이야기 내용 " + pStory.EffectValue + "개 증가)", Color.green);
            else
                Chat(system, " (이야기 내용 " + -pStory.EffectValue + "개 감소)", Color.green);
        }
        // 맵 횟수 증감
        if (pStory.EffectName == "MapCount")
        {
            //VillaManager.GetInstance().Energy += (int)pStory.EffectValue;
            if (pStory.EffectValue > 0)
                Chat(system, " (이야기 횟수 " + pStory.EffectValue + "회 증가)", Color.green);
            else
                Chat(system, " (이야기 횟수 " + -pStory.EffectValue + "회 감소)", Color.green);
        }
        // 몬스터 전투 확률
        if (pStory.EffectName == "MonsterRate")
        {
            //fightFrequencyMultiplier += pStory.EffectValue;
            Chat(system, " (몬스터와 전투 확률 " + pStory.EffectValue + "% 일시적 증가)", Color.green);
        }
    }
    private void Chat(IChattableObject pChattableObject, string pContent, Color? pFontColor = null, int pFontSize = 14)
    {
        tChat = new Chat(pChattableObject, pContent, pFontColor, pFontSize);
        chatRoom.AddChat(tChat);
    }
}
