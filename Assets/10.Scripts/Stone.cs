﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Tesseract;
using System.Collections;
using System.Collections.Generic;

public class Stone : MonoBehaviour
{
    [Header("Stone Values")]
    public StatusGenerator statusGenerator;
    private StatCollection statCollection = new StatCollection();
    private Status status;

    [SerializeField]
    private OreDefinition ore;

    public int Level { get; protected set; }
    
    [SerializeField]
    private Slider healthSlider;

    private void Awake()
    {
        status = statusGenerator.GenerateStatus();
        statCollection.AddStatus(status);

        statCollection.OnStatCollectionChanged += Notify_StoneHealthChanged;

        // UI      
        ChangeStoneColor();
    }

    // 채광하기(공격자 StatCollection)
    public void Mining(StatCollection pAttackStatCollection)
    {
        // 돌 공격
        DamageCalculator.Attack(pAttackStatCollection, statCollection);

        // 돌 부셔졌으면
        if (statCollection.IsDead)
        {
            // 레벨 업 (체력 증가, 체력 회복)
            LevelUp();
            
            // 광석 획득
            ExplodeOre();
        }
    }

    private void ExplodeOre()
    {
        List<GameObject> ore = new List<GameObject>();
        // todo: 광산 터지는 효과
        for(int i = 0; i < 5; i++)
        {
            var orePrefab = (GameObject)Resources.Load("Prefabs/Ore");
            var oreObject = Instantiate(orePrefab);
            oreObject.transform.SetParent(transform);
            var randomPosition = Random.insideUnitCircle;
            oreObject.transform.position = transform.position + new Vector3(randomPosition.x, randomPosition.y, 0) * 3 + Vector3.up * 3;
            ore.Add(oreObject);

            Rigidbody2DExtension.AddExplosionForce(oreObject.GetComponent<Rigidbody2D>(), 500, transform.position, 100);
        }
    }


    private void ChangeStoneColor()
    {
        GetComponent<SpriteRenderer>().color = ore.oreColor;
    }
    public void LevelUp()
    {
        Level += 1;
        status.SetLevel(Level);
        statCollection.Health.DefaultValue = (int)statCollection.MaxHealth;
        statCollection.Health.MaxValue = (int)statCollection.MaxHealth;        
        statCollection.Health.ResetValue();
    }
    
    private void Notify_StoneHealthChanged(StatCollection pStatCollection)
    {
        healthSlider.value = pStatCollection.Health.Value;
        healthSlider.maxValue = pStatCollection.MaxHealth;
    }
}


