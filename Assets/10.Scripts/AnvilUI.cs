﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AnvilUI : MonoBehaviour {

    [SerializeField]
    private Anvil anvil;

    [SerializeField]
    private Slider slider_Temperature;
    [SerializeField]
    private Text text_Temperature;
    [SerializeField]
    private Text text_SubTemperature;
    [SerializeField]
    private Text text_MaxDurability;
    [SerializeField]
    private Text text_TemperatureLevel;

    [SerializeField]
    private Slot itemSlot;

    [SerializeField]
    private Text text_hammerDamage;
    [SerializeField]
    private int[] textSize_ByTemperatureLevel;
    [SerializeField]
    private Color[] textColor_ByTemperatureLevel;

    private void Start()
    {
        anvil.OnItemChanged += UpdateUI_Anvil;

        slider_Temperature.maxValue = anvil.temperature.MaxValue;
        anvil.temperature.OnVarChanged += UpdateUI_Temperature;
        anvil.subTemperature.OnVarChanged += UpdateUI_SubTemperature;
    }

    public void OnClick_AnvilBtn()
    {
        if (anvil.Item == null)
        {
            anvil.SetTarget();

            ObjectManager.instance.inventoryUI.OpenInventory(ItemType.Equipment);
            ObjectManager.instance.inventoryUI.ClassifyItem(ItemType.Equipment);
            return;
        }
        else
        {
            // 망치질 하기
            anvil.Hammering();
            text_hammerDamage.text = "damage";
        }
    }

    private void UpdateUI_Temperature(Float_Var pStat)
    {
        // 용량 이미지 변경
        if (anvil.temperatureLevel != anvil.previousTemperatureLevel)
        {
            anvil.previousTemperatureLevel = anvil.temperatureLevel;
            text_hammerDamage.fontSize = textSize_ByTemperatureLevel[anvil.temperatureLevel];
            text_hammerDamage.color = textColor_ByTemperatureLevel[anvil.temperatureLevel];

            var temp = "";
            if (anvil.temperatureLevel == 1)
                temp = "식음";
            else if (anvil.temperatureLevel == 2)
                temp = "가열됨";
            else if (anvil.temperatureLevel == 3)
                temp = "뜨거움";
            else if (anvil.temperatureLevel == 4)
                temp = "과열됨";
            else if (anvil.temperatureLevel == 5)
                temp = "불타오름";

            text_TemperatureLevel.text = temp;            
        }
        slider_Temperature.value = pStat.Value;
        text_Temperature.text = "아이템 온도 : " + pStat.Value + " / " + pStat.MaxValue;
    }
    private void UpdateUI_SubTemperature(Float_Var pStat)
    {
        text_SubTemperature.text = "서브 온도 : " + pStat.Value + " / " + pStat.MaxValue;
    }
    private void UpdateUI_Anvil(ItemInfo pItem)
    {
        if (pItem == null)
        {
            itemSlot.RemoveSlot();
            text_MaxDurability.text = "";
        }
        else
        {
            itemSlot.SetSlot(pItem);
            text_MaxDurability.text = pItem.status.MaxHealth.Value.ToString();
        }
    }
}
