﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestInfo : MonoBehaviour {

    [SerializeField]
    private QuestDefinition questDefinition;

    private int level;
    private string ConditionName { get { return questDefinition.conditionName; } }
    private float ConditionValue { get { return questDefinition.conditionValue; } }

    public void SetQuest(QuestDefinition q)
    {
        questDefinition = q;
        // Notify delegate 연결
    }

    public void TakeQuest()
    {
        ObjectManager.instance.player.questList.Add(this);

        var condition = ConditionName;
        List<Furnace> furnaceList = new List<Furnace>();
        Anvil anvil = new Anvil();
        switch (condition)
        {
            
            // 용광로
            case "MeltingRatio":
                foreach (Furnace f in furnaceList)
                    f.meltingRatio.OnVarChanged += CheckFloatCondition;
                break;
            case "Temperature":
                foreach (Furnace f in furnaceList)
                    f.temperature.OnVarChanged += CheckFloatCondition;
                break;
            //case "Purity":                
            //    foreach (Furnace f in furnaceList)
            //        f.pureMass.Delegate_UpdateUI_Var += CheckDoubleCondition;
            //    break;

            // 모루&크래프트 아이템
            case "Durability":
                anvil.OnItemChanged += CheckItemCondition;
                break;
            case "Value":
                anvil.OnItemChanged += CheckItemCondition;
                break;

            //case "MeltingRatio":
            //    break;

            //case "MeltingRatio":
            //    break;
        }
    }

    private void OnDestroy()
    {
        var condition = ConditionName;
        if (condition == "MeltingRatio")
        {
            var list = FindObjectsOfType<Furnace>();
            foreach (Furnace f in list)
                f.meltingRatio.OnVarChanged += CheckFloatCondition;
        }
    }

    private void CheckFloatCondition(Float_Var f)
    {
        if (f.Value >= ConditionValue)
            Complete();
    }
    private void CheckDoubleCondition(Double_Var f)
    {

    }
    private void CheckItemCondition(ItemInfo pItem)
    {

    }

    private void Complete()
    {
        Debug.Log("Complete");
    }
}
