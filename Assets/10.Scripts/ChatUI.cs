﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tesseract;

public class ChatUI : MonoBehaviour {

    // UI Variables
    [Required, SerializeField]
    private Image image_Face;
    [Required, SerializeField]
    private Image image_ContentBackground;
    [Required, SerializeField]
    private Text text_PersonName;
    [Required, SerializeField]
    private Text text_Content;
    [Required, SerializeField]
    private Text text_Time;

    [SerializeField]
    private int maxWidth;

    public void SetChat(Chat pChat)
    {
        //Debug.Log(image_Face);
        //Debug.Log(pChat);
        //Debug.Log(pChat.chatPerson);
        //Debug.Log(pChat.chatPerson.Icon);
        image_Face.sprite = pChat.chatPerson.Icon;
        text_PersonName.text = pChat.chatPerson.Name;
        text_Content.text = pChat.contents;
        text_Content.fontSize = pChat.fontSize;
        text_Content.color = pChat.fontColor;
        text_Time.text = pChat.time.ToString();

        TextGenerator textGen = new TextGenerator();
        TextGenerationSettings textGenSettings = text_Content.GetGenerationSettings(text_Content.rectTransform.rect.size);
        float width = textGen.GetPreferredWidth(pChat.contents, textGenSettings);
        float height = textGen.GetPreferredHeight(pChat.contents, textGenSettings);

        if (width > maxWidth)
            width = maxWidth;

        image_ContentBackground.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);

        textGenSettings = text_Content.GetGenerationSettings(text_Content.rectTransform.rect.size);
        height = textGen.GetPreferredHeight(pChat.contents, textGenSettings);
        image_ContentBackground.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);


    }
}
