﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {

    private Button_Link currentButton;

    public void SetCurrentButton(Button_Link pButton)
    {
        if (currentButton == null)
        {
            currentButton = pButton;
        }
        currentButton.ResetButton();
        currentButton = pButton;
    }
}
