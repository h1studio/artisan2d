﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tesseract;

public class MapSelectUI : MonoBehaviour {
    
    private MapInfo map;

    [Required, SerializeField]
    private Text text_MapName;
    [Required, SerializeField]
    private Button btn_SetWay;
    [Required, SerializeField]
    private Button btn_Fight;
    [Required, SerializeField]
    private Button btn_Detail;
    [Required, SerializeField]
    private Button btn_Equip;

    private Text enemy;

    private void Start()
    {
        btn_SetWay.onClick.AddListener(OnClick_SetWay);
        btn_Fight.onClick.AddListener(OnClick_Fight);
        btn_Detail.onClick.AddListener(OnClick_Detail);
        btn_Equip.onClick.AddListener(OnClick_Equip);
    }

    public void SetMap(MapInfo pMap)
    {
        map = pMap;

        text_MapName.text = pMap.Name;
        //enemy.text = "";
    }

    private void OnClick_SetWay()
    {

    }
    private void OnClick_Fight()
    {
        MapController.instance.StoryStart(map);
    }
    private void OnClick_Detail()
    {

    }
    private void OnClick_Equip()
    {

    }

}
