﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TesterUI : MonoBehaviour {

    [SerializeField]
    private InputField InputField_OreAmount;
    [SerializeField]
    private Button button_AddOre;

    [SerializeField]
    private Button button_ProduceItem;
    [SerializeField]
    private Button button_ProduceDecoMaterial;
    [SerializeField]
    private Button button_ProduceCraftMaterial;

    [SerializeField]
    private InputField InputField_ChatContent;
    [SerializeField]
    private Button button_AddGeneralChat;
    [SerializeField]
    private Button button_AddMyChat;

    [SerializeField]
    private Button button_AddQuestChatRoom;
    [SerializeField]
    private Button button_AddAdventureChatRoom;

    [SerializeField]
    private Button button_SetTemperature;

    private void Start()
    {
        //button_AddOre.onClick.AddListener(AddOre);

        //button_ProduceItem.onClick.AddListener(ProduceItem);
        //button_ProduceDecoMaterial.onClick.AddListener(ProduceDecoMaterial);
        //button_ProduceCraftMaterial.onClick.AddListener(ProduceCraftMaterial);

        //button_AddGeneralChat.onClick.AddListener(AddGeneralChat);
        //button_AddMyChat.onClick.AddListener(AddMyChat);

        //button_AddQuestChatRoom.onClick.AddListener(AddQuestChatRoom);
        //button_AddAdventureChatRoom.onClick.AddListener(AddAdventureChatRoom);
        button_SetTemperature.onClick.AddListener(SetTemperature);        
    }    

    public void AddOre()
    {
        Tester.instance.AddOre(Convert.ToInt32(InputField_OreAmount.text));
    }
    public void ProduceItem()
    {
        Tester.instance.ProduceItem("구리 단검");
    }
    public void ProduceDecoMaterial()
    {
        Tester.instance.ProduceItem("한철");
    }
    public void ProduceCraftMaterial()
    {
        Tester.instance.ProduceItem("가고일 혈액");
    }
    public void AddGeneralChat()
    {
        Tester.instance.AddGeneralChat(InputField_ChatContent.text);
    }
    public void AddMyChat()
    {
        Tester.instance.AddMyChat(InputField_ChatContent.text);
    }
    public void AddQuestChatRoom()
    {
        Tester.instance.AddQuestChatRoom();
    }
    public void AddAdventureChatRoom()
    {
        Tester.instance.AddAdventureChatRoom();
    }
    public void SetTemperature()
    {
        Tester.instance.SetTemperature(2000);
    }
}
