﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Order : MonoBehaviour {

    OrderDefinition orderDefinition;

    public void Init(OrderDefinition pOrderDefinition)
    {
        orderDefinition = pOrderDefinition;
    }

    //public string Name { get { return orderDefinition.Name; } }
    public ItemInfo Item { get { return orderDefinition.Item; } }
}
