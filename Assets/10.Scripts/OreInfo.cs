﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreInfo {

    public OreDefinition ore;
    
    public Int_Var amount;

    public OreInfo(OreDefinition pOre, int pAmount = 1, int pMaxAmount = 99999)
    {
        ore = pOre;
        amount = new Int_Var(pAmount, pMaxAmount, 1);
    }
    
    public OreInfo(string pOreName, int pAmount = 1, int pMaxAmount = 99999)
    {
        foreach (OreDefinition tOre in DefinitionList_Database.instance.oreList)
        {
            if (tOre.Name == pOreName)
            {
                ore = tOre;
                amount = new Int_Var(pAmount, pMaxAmount, 1);
                return;
            }
        }
    }

    public void AddOre(OreInfo pOre)
    {
        if (pOre.ore.ID == ore.ID)
            amount.ChangeValue(pOre.amount.Value);
    }

    public bool ReduceOre(OreInfo pOre)
    {
        if (pOre.ore.ID == ore.ID)
        {
            if (amount.Value - pOre.amount.Value >= 0)
            {
                amount.ChangeValue(-pOre.amount.Value);
                return true;
            }
            else
                return false;
        }
        return false;
    }
}
