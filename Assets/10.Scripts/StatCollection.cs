﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class StatCollection {

    private List<IStatus> statusList = new List<IStatus>();

    public event Action<StatCollection> OnStatCollectionChanged;

    public Int_Var Health { get; protected set; }

    public StatCollection()
    {
        Health = new Int_Var((int)MaxHealth, (int)MaxHealth);
        Health.OnVarChanged += UpdateUI_HealthChanged;
        Notify_StatCollectionChanged();
    }

    public void AddStatus(IStatus pStatus)
    {
        statusList.Add(pStatus);
        Health.MaxValue += (int)pStatus.MaxHealth.Value;
        Health.ChangeValue((int)pStatus.MaxHealth.Value);
        pStatus.OnStatusChanged += UpdateUI_Status;
    }
    public void RemoveStatus(IStatus pStatus)
    {
        statusList.Remove(pStatus);
        Health.MaxValue -= (int)pStatus.MaxHealth.Value;
        Health.ChangeValue(-(int)pStatus.MaxHealth.Value);
        pStatus.OnStatusChanged -= UpdateUI_Status;
    }

    public bool IsDead { get { return Health.Value <= 0; } }

    public float MaxHealth
    {
        get
        {
            var value = 0f;
            var factor = 1f;
            foreach(IStatus status in statusList)
            {
                value += status.MaxHealth.Value;
                factor *= status.MaxHealth.GlobalFactor;
            }
            return value * factor;
        }
    }

    public float Damage
    {
        get
        {
            var value = 0f;
            var factor = 1f;
            foreach (IStatus status in statusList)
            {
                value += status.Damage.Value;
                factor *= status.Damage.GlobalFactor;
            }
            return value * factor;
        }
    }

    public float Defence
    {
        get
        {
            var value = 0f;
            var factor = 1f;
            foreach (IStatus status in statusList)
            {
                value += status.Defence.Value;
                factor *= status.Defence.GlobalFactor;
            }
            return value * factor;
        }
    }

    public float AttackSpeed
    {
        get
        {
            var value = 0f;
            var factor = 1f;
            foreach (IStatus status in statusList)
            {
                value += status.AttackSpeed.Value;
                factor *= status.AttackSpeed.GlobalFactor;
            }
            return value * factor;
        }
    }

    public float CriticalProbability
    {
        get
        {
            var value = 0f;
            var factor = 1f;
            foreach (IStatus status in statusList)
            {
                value += status.CriticalProbability.Value;
                factor *= status.CriticalProbability.GlobalFactor;
            }
            return value * factor;
        }
    }

    public float CriticalMultiplier
    {
        get
        {
            var value = 0f;
            var factor = 1f;
            foreach (IStatus status in statusList)
            {
                value += status.CriticalMultiplier.Value;
                factor *= status.CriticalMultiplier.GlobalFactor;
            }
            return value* factor;
        }
    }
    

    
    private void UpdateUI_Status(IStatus pStatus)
    {
        Notify_StatCollectionChanged();
    }
    private void UpdateUI_HealthChanged(Int_Var pVar)
    {
        Notify_StatCollectionChanged();
    }
    private void Notify_StatCollectionChanged()
    {
        if (OnStatCollectionChanged != null)
            OnStatCollectionChanged(this);
    }
}
