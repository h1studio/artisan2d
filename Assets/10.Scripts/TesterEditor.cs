﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Tester))]
[CanEditMultipleObjects]
public class TesterEditor : Editor
{
    Tester tester;

    //private bool showItemSettings = true;
    //SerializedProperty isForged;
    //SerializedProperty purity;
    //SerializedProperty durability;

    //private int itemIndex;
    //private int moneyValue = 10000;
    //private int itemValue = 1;
    //private int levelUpValue = 1;



    //void OnEnable()
    //{
    //    tester = target as Tester;

    //    isForged = serializedObject.FindProperty("isForged");
    //    purity = serializedObject.FindProperty("purity");
    //    durability = serializedObject.FindProperty("durability");
    //}
    
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        Game();
        Mine();
        Smelt();        

        //AddOreGUI();
        //ProduceItemGUI();
    }

    private void Mine()
    {
        GUILayout.Label("Mine");

        EditorGUILayout.BeginHorizontal();
        var amount = EditorGUILayout.IntField(100, GUILayout.Width(100));
        if (GUILayout.Button("Add Ore"))
        {
            Tester.instance.AddOre(amount);
        }

        EditorGUILayout.EndHorizontal();
    }
    private void Smelt()
    {
        GUILayout.Label("Smelt");

        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        var temperature = EditorGUILayout.FloatField(2000, GUILayout.Width(100));
        if (GUILayout.Button("Temperature"))
        {
            Tester.instance.SetTemperature(temperature);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        var meltingRatio = EditorGUILayout.FloatField(100, GUILayout.Width(100));
        if (GUILayout.Button("MeltingRatio"))
        {
            Tester.instance.SetMeltingRatio(meltingRatio);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        var itemName = EditorGUILayout.TextField("구리 단검", GUILayout.Width(100));
        if (GUILayout.Button("Item"))
        {
            Tester.instance.ProduceItem(itemName);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();
    }
    private void Game()
    {
        GUILayout.Label("Game");
        EditorGUILayout.BeginHorizontal();
        var gameSpeed = EditorGUILayout.IntField(5, GUILayout.Width(100));
        if (GUILayout.Button("GameSpeed"))
        {
            Tester.instance.SetGameSpeed(gameSpeed);
        }
        EditorGUILayout.EndHorizontal();
    }


    //void AddOreGUI()
    //{
    //    GUILayout.Label("Add Ore:");

    //    EditorGUILayout.BeginHorizontal();
    //    int oreAmount = EditorGUILayout.IntField("", 100, GUILayout.Width(100));
    //    GUI.color = Color.green;
    //    if (GUILayout.Button("Add Ore"))                                                                                   //creating button with name "AddItem"
    //    {
    //        Tester.instance.AddOre(oreAmount);
    //    }
    //    GUI.color = Color.white;
    //    EditorGUILayout.EndHorizontal();
    //}
    //void ProduceItemGUI()
    //{
    //    GUILayout.Label("Produce Item:");

    //    EditorGUILayout.BeginHorizontal();
    //    GUI.color = Color.green;
    //    if (GUILayout.Button("Produce Item"))                                                                                   //creating button with name "AddItem"
    //    {
    //        //Tester.instance.ProduceItem();
    //    }
    //    GUI.color = Color.white;
    //    EditorGUILayout.EndHorizontal();
    //}

    //void AddMoneyGUI()                                                                                                       //add a item to the inventory through the inspector
    //{
    //    GUILayout.Label("Add Money:");
    //    //space to the top gui element
    //    EditorGUILayout.BeginHorizontal();                                                                                  //starting horizontal GUI elements

    //    moneyValue = EditorGUILayout.IntField("", moneyValue, GUILayout.Width(100));
    //    GUI.color = Color.green;                                                                                            //set the color of all following guielements to green
    //    if (GUILayout.Button("Add Money"))                                                                                   //creating button with name "AddItem"
    //    {
    //        PlayerManager.GetInstance().AddMoney(moneyValue);
    //    }
    //    GUI.color = Color.white;
    //    EditorGUILayout.EndHorizontal();                                                                                    //end the horizontal gui layout
    //}

    //void LevelUp()
    //{
    //    GUILayout.Label("Level Up:");
    //    //space to the top gui element
    //    EditorGUILayout.BeginHorizontal();                                              //create a popout with all itemnames in it and save the itemID of it
    //    levelUpValue = EditorGUILayout.IntField("", levelUpValue, GUILayout.Width(100));
    //    GUI.color = Color.green;                                                                                            //set the color of all following guielements to green
    //    if (GUILayout.Button("Level Up"))                                                                                   //creating button with name "AddItem"
    //    {
    //        for (int i = 0; i < levelUpValue; i++)
    //        {
    //            PlayerManager.GetInstance().LevelUp();
    //        }
    //    }
    //    GUI.color = Color.white;
    //    EditorGUILayout.EndHorizontal();
    //}

    //void AddUnforgedItemGUI()
    //{
    //    GUILayout.Label("Add UnForged Item:");
    //    //space to the top gui element
    //    EditorGUILayout.BeginVertical("Box");
    //    EditorGUILayout.BeginHorizontal();                                                                                  //starting horizontal GUI elements
    //    ItemList_Database inventoryItemList = (ItemList_Database)Resources.Load("Database/ItemList");                            //loading the itemdatabase
    //    string[] items = new string[inventoryItemList.itemList.Count];                                                      //create a string array in length of the itemcount
    //    for (int i = 0; i < inventoryItemList.itemList.Count; i++)                                                                              //go through the item array
    //    {
    //        if (1000 <= inventoryItemList.itemList[i].itemID && inventoryItemList.itemList[i].itemID <= 1999)
    //            items[i] = inventoryItemList.itemList[i].GetName();                                                              //and paste all names into the array
    //    }
    //    itemIndex = EditorGUILayout.Popup("", itemIndex, items, EditorStyles.popup);                                              //create a popout with all itemnames in it and save the itemID of it
    //    itemValue = EditorGUILayout.IntField("", itemValue, GUILayout.Width(40));
    //    GUI.color = Color.green;                                                                                            //set the color of all following guielements to green
    //    if (GUILayout.Button("Add Item"))                                                                                   //creating button with name "AddItem"
    //    {
    //        for (int i = 0; i < itemValue; i++)
    //        {
    //            Item item = inventoryItemList.GetItemByIndex(itemIndex);
    //            item.IsForged = false;
    //            item.SetPurity(99f);
    //            item.Durability = 100;
    //            item.CurDurability = 100;
    //            InventoryManager.GetInstance().AddItem(item);
    //        }
    //    }
    //    GUI.color = Color.white;
    //    EditorGUILayout.EndHorizontal();
    //    EditorGUILayout.EndVertical();
    //}
    //void AddForgedItemGUI()
    //{
    //    GUILayout.Label("Add Forged Item:");
    //    //space to the top gui element
    //    EditorGUILayout.BeginVertical("Box");
    //    EditorGUILayout.BeginHorizontal();                                                                                  //starting horizontal GUI elements
    //    ItemList_Database inventoryItemList = (ItemList_Database)Resources.Load("Database/ItemList");                            //loading the itemdatabase
    //    string[] items = new string[inventoryItemList.itemList.Count];                                                      //create a string array in length of the itemcount
    //    for (int i = 0; i < inventoryItemList.itemList.Count; i++)                                                                              //go through the item array
    //    {
    //        if (1000 <= inventoryItemList.itemList[i].itemID && inventoryItemList.itemList[i].itemID <= 1999)
    //            items[i] = inventoryItemList.itemList[i].GetName();                                                             //and paste all names into the array
    //    }
    //    itemIndex = EditorGUILayout.Popup("", itemIndex, items, EditorStyles.popup);                                              //create a popout with all itemnames in it and save the itemID of it
    //    itemValue = EditorGUILayout.IntField("", itemValue, GUILayout.Width(40));
    //    GUI.color = Color.green;                                                                                            //set the color of all following guielements to green
    //    if (GUILayout.Button("Add Item"))                                                                                   //creating button with name "AddItem"
    //    {
    //        for (int i = 0; i < itemValue; i++)
    //        {
    //            Item item = inventoryItemList.GetItemByIndex(itemIndex);
    //            item.IsForged = true;
    //            item.SetPurity(99f);
    //            item.Durability = 100;
    //            item.CurDurability = 100;
    //            InventoryManager.GetInstance().AddItem(item);
    //        }
    //    }
    //    GUI.color = Color.white;
    //    EditorGUILayout.EndHorizontal();
    //    EditorGUILayout.EndVertical();
    //}
    //void AddMaterialItemGUI()
    //{
    //    GUILayout.Label("Add Material Item:");
    //    //space to the top gui element
    //    EditorGUILayout.BeginVertical("Box");
    //    EditorGUILayout.BeginHorizontal();                                                                                  //starting horizontal GUI elements
    //    ItemList_Database inventoryItemList = (ItemList_Database)Resources.Load("Database/ItemList");                            //loading the itemdatabase
    //    string[] items = new string[inventoryItemList.itemList.Count];                                                      //create a string array in length of the itemcount
    //    for (int i = 0; i < inventoryItemList.itemList.Count; i++)                                                                              //go through the item array
    //    {
    //        if (1000 > inventoryItemList.itemList[i].itemID || inventoryItemList.itemList[i].itemID > 1999)
    //            items[i] = inventoryItemList.itemList[i].GetName();                                                               //and paste all names into the array
    //    }
    //    itemIndex = EditorGUILayout.Popup("", itemIndex, items, EditorStyles.popup);                                              //create a popout with all itemnames in it and save the itemID of it
    //    itemValue = EditorGUILayout.IntField("", itemValue, GUILayout.Width(40));
    //    GUI.color = Color.green;                                                                                            //set the color of all following guielements to green
    //    if (GUILayout.Button("Add Item"))                                                                                   //creating button with name "AddItem"
    //    {
    //        for (int i = 0; i < itemValue; i++)
    //        {
    //            Item item = inventoryItemList.GetItemByIndex(itemIndex);
    //            item.IsForged = true;
    //            item.SetPurity(99f);
    //            item.Durability = 100;
    //            item.CurDurability = 100;
    //            InventoryManager.GetInstance().AddItem(item);
    //        }
    //    }
    //    GUI.color = Color.white;
    //    EditorGUILayout.EndHorizontal();
    //    EditorGUILayout.EndVertical();
    //}
}
#endif
