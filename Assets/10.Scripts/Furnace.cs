﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class Furnace : MonoBehaviour{

    public event Action<OreDefinition> OnOreChanged;

    public Float_Var temperature = new Float_Var(50f, 2000f,1);
    public Float_Var subTemperature = new Float_Var(0f, 300f,1);
    public Float_Var meltingRatio = new Float_Var(0f, 100f,1);

    [SerializeField]
    private float temperatureUpSpeed = 100f;

    private OreDefinition _curOre;
    public OreDefinition CurOre
    {
        get { return _curOre; }
        set
        {
            _curOre = value;
            Notify_OreChanged();
        }
    }

    public float maxMass = 10f;

    [HideInInspector]
    public Double_Var pureMass = new Double_Var("PureMass", 0f);
    [HideInInspector]
    public Double_Var impureMass = new Double_Var("ImpureMass", 0f);



    public double Mass
    {
        get { return pureMass.value + impureMass.value; }
    }
    public double Purity
    {
        get
        {
            if (Mass == 0)
                return 0f;
            else
                return pureMass.value / Mass;
        }
    }



    private void Update()
    {
        IncreaseTemperature();
        IncreaseMeltingRatio();
    }

    public bool CheckToAddOre(OreInfo pOre)
    {
        // 기존에 들어가있는 광석종류 확인
        if (CurOre != null && CurOre != pOre.ore)
        {
            Debug.Log("투입된 다른 종류의 광석이 있습니다.\n투입된 광석 : " + CurOre.Name + ", 투입하려는 광석 : " + pOre.ore.Name);
            return false;
        }

        // 들어갈 공간이 있는지 확인
        if (maxMass - Mass < pOre.amount.Value)
        {
            Debug.Log("용광로의 공간이 부족합니다.\n투입되려는 질량 : " + pOre.amount + "용광로 여유공간 : " + (maxMass - Mass));
            return false;
        }

        return true;
    }

    public void AddOre(OreInfo pOre)
    {
        // 광석 투입
        CurOre = pOre.ore;
        pureMass.ChangeValue(pOre.ore.orePurity * pOre.amount.Value);
        impureMass.ChangeValue((1 - pOre.ore.orePurity) * pOre.amount.Value);
    }
    
    private void IncreaseTemperature()
    {
        if (subTemperature.Value > 0f)
        {
            float changeValue = Time.deltaTime * temperatureUpSpeed;
            if (subTemperature.Value < changeValue)
                changeValue = subTemperature.Value;

            temperature.ChangeValue(changeValue);
            subTemperature.ChangeValue(-changeValue);
        }
    }

    private void IncreaseMeltingRatio()
    {
        if(temperature.Value >= 1200 && CurOre != null)
        {
            float changeValue = Time.deltaTime * 5f;

            meltingRatio.ChangeValue(changeValue);
        }
    }

    private void Notify_OreChanged()
    {
        if (OnOreChanged != null)
            OnOreChanged(CurOre);
    }
    

    //private void Notify_FurnaceChanged()
    //{
    //    text_Description.text = "1";
    //}
}
