﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : MonoBehaviour {
    
    private WorkerState state = WorkerState.Rest;
    
    private Mine mine;

    [SerializeField]
    private Sprite _image;
    public Sprite Image { get { return _image; } }

    [SerializeField]
    private string _name;
    public string Name { get { return _name; } }

    public StatusGenerator statusGenerator;
    private StatCollection statCollection = new StatCollection();
    private Status status;
    
    private void Awake()
    {
        status = statusGenerator.GenerateStatus();
        statCollection.AddStatus(status);
    }

    public void Move(Transform pPosition)
    {
        transform.SetParent(pPosition);
        transform.position = pPosition.position;
    }

    public void Work()
    {
        if(MineManager.instance.CurMine.SetWorker(this))
        {
            mine = MineManager.instance.CurMine;
            state = WorkerState.Work;

            Notify_StateChanged();
        }
    }
    public void Rest()
    {
        Debug.Log("Rest");
        mine.RemoveWorker(this);
        mine = null;
        
        state = WorkerState.Rest;
        Notify_StateChanged();
    }

    private IEnumerator Working()
    {
        Debug.Log("Working");
        while (state == WorkerState.Work)
        {            
            var attackDelay = 1 / statCollection.AttackSpeed;
            yield return new WaitForSeconds(attackDelay);
            mine.stone.Mining(statCollection);
        }
    }

    private void Notify_StateChanged()
    {
        UpdateUI_State();
    }

    private void UpdateUI_State()
    {
        if (state == WorkerState.Work)
            StartCoroutine(Working());
        else if (state == WorkerState.Rest)
            StopAllCoroutines();        
    }    
}

public enum WorkerState
{ 
    Rest = 0,
    Work = 1
}
