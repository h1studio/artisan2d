﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonSetting : MonoBehaviour {

    [SerializeField]
    private List<GameObject> openPanel = new List<GameObject>();
    [SerializeField]
    private List<GameObject> closePanel = new List<GameObject>();
    [SerializeField]
    private bool changeButtonColor;          // 버튼 색깔 바꾸기
    [SerializeField]
    private Color _changedColor;
    protected Color changedColor
    {
        get { return _changedColor; }
        set { _changedColor = value; }
    }
    private Color originColor;

    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnClick_Button);
        originColor = GetComponent<Image>().color;
    }

    protected virtual void OnClick_Button()
    {
        ClosePanel();
    }

    public void ResetButton()
    {
        // 패널 닫기
        if (openPanel.Count != 0)
        {
            ActivePanel(false);
        }

        // 버튼 색깔 초기화
        SetButtonColor(originColor);
    }

    protected void ActivePanel(bool b)
    {
        foreach (GameObject g in openPanel)
        {
                g.SetActive(b);
        }     
    }
    protected void SetButtonColor(Color pColor)
    {
        if (changeButtonColor)
        {
            GetComponent<Image>().color = pColor;
        }
    }

    private void ClosePanel()
    {
        foreach (GameObject g in closePanel)
        {
                g.SetActive(false);
        }
    }

}
