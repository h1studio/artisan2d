﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftUI : MonoBehaviour {

    [SerializeField]
    private Craft craft;

    [SerializeField]
    private Slot itemSlot;
    [SerializeField]
    private Slot decoMaterialSlot;
    [SerializeField]
    private Slot craftMaterialSlot;

    [SerializeField]
    private Button btn_Deco;

    private void Start()
    {
        craft.Delegate_UpdateUI_CraftItem += UpdateUI_Anvil;
        itemSlot.GetComponent<Button>().onClick.AddListener(OnClick_ItemSlot);
        decoMaterialSlot.GetComponent<Button>().onClick.AddListener(OnClick_DecoMaterialSlot);
        craftMaterialSlot.GetComponent<Button>().onClick.AddListener(OnClick_CraftMaterialSlot);

        btn_Deco.onClick.AddListener(OnClick_DecoBtn);
    }

    public void OnClick_DecoBtn()
    {
        craft.DecorateItem();
    }
    public void OnClick_CraftBtn()
    {

    }
    public void OnClick_EngraveBtn()
    {

    }
    //todo: 다른 슬롯 클릭시 다른 인벤토리 열리도록 수정, 검은색으로 분류되는것도 고려
    public void OnClick_ItemSlot()
    {
        craft.SetTarget();

        ObjectManager.instance.inventoryUI.OpenInventory(ItemType.Equipment);
        ObjectManager.instance.inventoryUI.ClassifyItem(ItemType.Equipment);
    }
    public void OnClick_DecoMaterialSlot()
    {
        craft.SetTarget();

        ObjectManager.instance.inventoryUI.OpenInventory(ItemType.Material);
        ObjectManager.instance.inventoryUI.ClassifyItem(ItemType_Refined.Material_Decorate);
    }
    public void OnClick_CraftMaterialSlot()
    {
        craft.SetTarget();

        ObjectManager.instance.inventoryUI.OpenInventory(ItemType.Material);
        ObjectManager.instance.inventoryUI.ClassifyItem(ItemType_Refined.Material_Craft);
    }

    private void UpdateUI_Anvil()
    {
        itemSlot.SetSlot(craft.item);
        decoMaterialSlot.SetSlot(craft.decoMaterial);
        craftMaterialSlot.SetSlot(craft.craftMaterial);

        //if (craft.item == null)
        //{
        //    itemSlot.RemoveItem();
        //    //text_MaxDurability.text = "";
        //}
        //else
        //{
            
        //    //text_MaxDurability.text = craft.item.maxHealth.ToString();
        //}

    }
}
