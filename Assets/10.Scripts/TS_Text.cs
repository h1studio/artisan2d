﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TS_Text : MonoBehaviour {

    [SerializeField]
    private Text _text_Label;
    public Text text_Label { get { return _text_Label; } }

    [SerializeField]
    private Text _text_Content;
    public Text text_Content { get { return _text_Content; } } 
        
    public void SetLabel(string pLabelText)
    {
        text_Label.text = pLabelText;
    }

    public void SetContent(string pText)
    {
        text_Content.text = pText;
    }    
    public void SetContent(int pText)
    {
        SetContent(pText.ToString());
    }
    public void SetContent(float pText)
    {
        SetContent(pText.ToString());
    }
}
