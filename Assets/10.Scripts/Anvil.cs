﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Anvil : MonoBehaviour, ISetItemFromInventory
{
    public ItemInfo Item { get; private set; }

    public event Action<ItemInfo> OnItemChanged;
    
    public Float_Var temperature = new Float_Var(50f, 2000f, 1);
    public Float_Var subTemperature = new Float_Var(0f, 300f, 1);

    [SerializeField]
    private int[] _temperatureLevelBoundary;
    public int[] temperatureLevelBoundary { get { return _temperatureLevelBoundary; } }

    [SerializeField]
    private float[] effect_ByTemperatureLevel;

    [SerializeField]
    private float temperatureUpSpeed = 100f;

    [HideInInspector]
    public int previousTemperatureLevel = 0;
    public int temperatureLevel { get { return TS_Math.GetLevel(temperature.Value, temperatureLevelBoundary); } }
        
    public void SetTarget()
    {
        ObjectManager.instance.player.inventory.SetTarget(this);
    }
    
    public void SetItem(ItemInfo pItem)
    {
        Item = pItem;
        Notify_ItemChanged(Item);
    }
    public void RemoveItem()
    {
        Item = null;
        Notify_ItemChanged(Item);
    }

    private bool canHammering
    {
        get { return Item != null; }
    }

    public void Hammering()
    {
        if(canHammering)
        {
            // 온도에 따른 증가수치적용
            //내구도 증가
            Item.status.MaxHealth.AddStat(1);

            //가치 증가

            Notify_ItemChanged(Item);
        }
    }

    private void Update()
    {
        IncreaseTemperature();
    }

    private void IncreaseTemperature()
    {
        if (subTemperature.Value > 0f)
        {
            float changeValue = Time.deltaTime * temperatureUpSpeed;
            if (subTemperature.Value < changeValue)
                changeValue = subTemperature.Value;

            temperature.ChangeValue(changeValue);
            subTemperature.ChangeValue(-changeValue);
        }
    }

    public void Done()
    {
        //SoundManager.GetInstance().PlaySound_DoneItem();

        //GetItem().CurDurability = GetItem().Durability;

        //MessageManager.GetInstance().CallLargePanel("제작완료", "'" + GetItem().GetName() + "'을 제작하였습니다."
        //    + "\n- 순도 : " + GetItem().GetItemPurity().ToString() + "%"
        //    + "\n- 내구도 : " + TS_Math.Round(GetItem().GetDurability(), 2).ToString()
        //    + "\n- 가치 : " + TS_Math.Round(GetItem().GetValue(), 2).ToString());

        //// 아이템 제거
        //CraftManager.GetInstance().GetComponent<Inventory>().AddItem(GetItem());
        //ForgingManager.GetInstance().GetComponent<Inventory>().RemoveItem(GetItem());
        //RemoveItem();
        //StopAllCoroutines();
    }

    public void Notify_ItemChanged(ItemInfo pItem)
    {
        if (OnItemChanged != null)
            OnItemChanged(pItem);
    }
}
