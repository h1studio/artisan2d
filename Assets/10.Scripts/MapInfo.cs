﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapInfo {
    
    public MapDefinition mapDefinition;
    
    public MapUI mapUI { get; set; }

    public string Name { get { return mapDefinition.Name; } }
    public string IntroText { get { return mapDefinition.IntroText; } }
    public AdventureType AdventureType { get { return mapDefinition.AdventureType; } }
    public EnemyDefinition Enemy { get { return mapDefinition.enemyList[Random.Range(0, mapDefinition.enemyList.Count)]; } }

    public MapInfo(MapDefinition pMap)
    {
        mapDefinition = pMap;        
    }

    public bool IsLinked(MapInfo pMap)
    {
        return mapDefinition.linkedMapList.Contains(pMap.mapDefinition);
    }

    
}
