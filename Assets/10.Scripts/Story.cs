﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryDefinition {

    [SerializeField]
    private string _content;
    public string Content { get { return _content; } set { _content = value; } }

    [SerializeField]
    private string _effectName;
    public string EffectName { get { return _effectName; } set { _effectName = value; } }

    [SerializeField]
    private float _effectValue;
    public float EffectValue { get { return _effectValue; } set { _effectValue = value; } }
}

public enum StoryType
{
    None = 0,
    Fight = 1,
    MapExplore = 2,
    Treasure = 3,
    Condition = 4,
    Trap = 5,
    Colleague = 6,
    MonsterCorpse = 7,
    Building = 8,
    Animal = 9,
    Path = 10,
}
