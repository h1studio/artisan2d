﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderSheet : MonoBehaviour {

    public Order order { get; private set; }

    [SerializeField]
    private Slot itemImageSlot;
    [SerializeField]
    private Text name_Text;
    [SerializeField]
    private Text durability_Text;
    [SerializeField]
    private Text purity_Text;

    [SerializeField]
    private Color retailerOrderColor;
    [SerializeField]
    private Color luxuryOrderColor;
    [SerializeField]
    private Color advOrderColor;
    [Space]
    [SerializeField]
    private Color retailerOrderColor_Clicked;
    [SerializeField]
    private Color luxuryOrderColor_Clicked;
    [SerializeField]
    private Color advOrderColor_Clicked;
    
    public void SetOrder(Order pOrder)
    {
        order = pOrder;
        UpdateUI();
    }

    public void UpdateUI_Color(bool isClicked)
    {
        if (isClicked)
            GetComponent<Image>().color = retailerOrderColor_Clicked;
        else
            GetComponent<Image>().color = retailerOrderColor;
    }

    private void UpdateUI()
    {
        itemImageSlot.SetSlot(order.Item);        
        name_Text.text = order.Item.Name;
        //durability_Text.text = TS_Math.Round(order.Item.damage, 0).ToString();
        //purity_Text.text = order.item.GetItemPurity().ToString() + " %";
    }
}
