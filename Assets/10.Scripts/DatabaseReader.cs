﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

public class DatabaseReader : MonoBehaviour {

    [ContextMenu("Read_All")]
    public void Read_All()
    {
        Read_OreList();
        Read_FrameList();
        Read_ItemList();
        Read_AffixList();
        Read_MapList();
        Read_EnemyList();
        Read_OrderList();
        Read_CustomerList();

        Read_Map_Enemy_Relation();
        Read_Map_Map_Relation();
        Read_Item_Affix_Relation();
        //Read_Customer_Order_Relation();

    }

    [ContextMenu("Read_OreList")]
    public void Read_OreList()
    {
        List<Dictionary<string, object>> data = CSVReader.Read("OreList");

        if(data.Count != 0)
        {
            List<OreDefinition> list = CreateAsset<OreDefinition>(data.Count, "Assets/10.Scripts/Database/Ore/Ore");
            
            DefinitionList_Database.instance.oreList = new List<OreDefinition>();
            for (int i = 0; i < data.Count; i++)
            {
                bool b = false;
                foreach(OreDefinition o in list)
                {
                    if(o.ID == Convert.ToUInt32(data[i]["ID"]))
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(o), Convert.ToString(data[i]["ScriptName"]) + ".asset");
                        o.Name = Convert.ToString(data[i]["Name"]);
                        o.orePurity = Convert.ToSingle(data[i]["Purity"]);
                        o.SetColor(Convert.ToString(data[i]["Color"]));

                        DefinitionList_Database.instance.oreList.Add(o);
                        b = true;
                        break;
                    }
                }
                if(!b)
                    Debug.LogWarning("해당 ID에 맞는 것이 없습니다." + Convert.ToUInt32(data[i]["ID"]));
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    [ContextMenu("Read_FrameList")]
    public void Read_FrameList()
    {
        List<Dictionary<string, object>> data = CSVReader.Read("FrameList");

        if (data.Count != 0)
        {
            List<FrameDefinition> list = CreateAsset<FrameDefinition>(data.Count, "Assets/10.Scripts/Database/Frame/Frame");

            DefinitionList_Database.instance.frameList = new List<FrameDefinition>();
            for (int i = 0; i < data.Count; i++)
            {
                bool b = false;
                foreach (FrameDefinition o in list)
                {
                    if (o.ID == Convert.ToUInt32(data[i]["ID"]))
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(o), Convert.ToString(data[i]["ScriptName"]) + ".asset");
                        o.name = Convert.ToString(data[i]["ScriptName"]);
                        o.Name = Convert.ToString(data[i]["Name"]);

                        DefinitionList_Database.instance.frameList.Add(o);
                        b = true;
                        break;
                    }
                }
                if (!b)
                    Debug.LogWarning("해당 ID에 맞는 것이 없습니다." + Convert.ToUInt32(data[i]["ID"]));
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    [ContextMenu("Read_ItemList")]
    public void Read_ItemList()
    {
        List<Dictionary<string, object>> data = CSVReader.Read("ItemList");

        if (data.Count != 0)
        {
            List<ItemDefinition> list = CreateAsset<ItemDefinition>(data.Count, "Assets/10.Scripts/Database/Item/Item");

            DefinitionList_Database.instance.itemList = new List<ItemDefinition>();
            for (int i = 0; i < data.Count; i++)
            {
                bool b = false;
                foreach (ItemDefinition o in list)
                {
                    if (o.ID == Convert.ToUInt32(data[i]["ID"]))
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(o), Convert.ToString(data[i]["ScriptName"]) + ".asset");
                        o.Name = Convert.ToString(data[i]["Name"]);
                        o.Description = Convert.ToString(data[i]["Description"]);
                        //itemList[i].icon = Convert.ToString(data[i]["Icon"]);

                        o.ItemType = EnumDatabase.GetEnumValue<ItemType>(Convert.ToString(data[i]["ItemType"]));
                        o.ItemType_Refined = EnumDatabase.GetEnumValue<ItemType_Refined>(Convert.ToString(data[i]["RefinedType"]));

                        o.MaxStackSize = Convert.ToUInt32(data[i]["MaxStack"]);
                        o.BuyPrice = Convert.ToInt32(data[i]["BuyPrice"]);
                        o.SellPrice = Convert.ToInt32(data[i]["SellPrice"]);
                        o.IsSellable = Convert.ToBoolean(data[i]["IsSellable"]);

                        o.CanProduce = Convert.ToBoolean(data[i]["CanProduce"]);
                        if (o.CanProduce)
                        {
                            o.producable = new Producable
                                (
                                Convert.ToString(data[i]["Frame"]),
                                Convert.ToString(data[i]["Ore"])
                                );
                        }
                        else
                            o.producable = null;

                        //스테이터스
                        o.canFight = Convert.ToBoolean(data[i]["CanFight"]);
                        if (o.canFight)
                        {
                            o.statusGenerator = new StatusGenerator
                                (
                                Convert.ToInt32(data[i]["MaxHealth"]),
                                Convert.ToInt32(data[i]["Damage"]),
                                Convert.ToInt32(data[i]["Defence"]),
                                Convert.ToSingle(data[i]["AttackSpeed"]),
                                Convert.ToSingle(data[i]["CriticalProbability"]),
                                Convert.ToSingle(data[i]["CriticalMultiplier"])
                                );
                        }
                        //else
                        //    o.statusGenerator = null;


                        DefinitionList_Database.instance.itemList.Add(o);
                        b = true;
                        break;
                    }
                }
                if (!b)
                    Debug.LogWarning("해당 ID에 맞는 것이 없습니다." + Convert.ToUInt32(data[i]["ID"]));
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    [ContextMenu("Read_AffixList")]
    public void Read_AffixList()
    {
        List<Dictionary<string, object>> data = CSVReader.Read("AffixList");

        if (data.Count != 0)
        {
            List<AffixDefinition> list = CreateAsset<AffixDefinition>(data.Count, "Assets/10.Scripts/Database/Affix/Affix");

            DefinitionList_Database.instance.affixList = new List<AffixDefinition>();
            for (int i = 0; i < data.Count; i++)
            {
                bool b = false;
                foreach (AffixDefinition o in list)
                {
                    if (o.ID == Convert.ToUInt32(data[i]["ID"]))
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(o), Convert.ToString(data[i]["ScriptName"]) + ".asset");
                        o.Name = Convert.ToString(data[i]["Name"]);
                        o.description = Convert.ToString(data[i]["Description"]);
                        //itemList[i].icon = Convert.ToString(data[i]["Icon"]);
                        string type = Convert.ToString(data[i]["Type"]);

                        o.affixType = EnumDatabase.GetEnumValue<AffixType>(Convert.ToString(data[i]["Type"]));

                        o.tier = Convert.ToInt32(data[i]["Tier"]);

                        //스테이터스
                        o.status = new Status
                            (
                            //todo : Attack타입
                            Convert.ToInt32(data[i]["MaxHealth"]),
                            Convert.ToInt32(data[i]["Damage"]),
                            Convert.ToInt32(data[i]["Defence"]),
                            Convert.ToSingle(data[i]["AttackSpeed"]),
                            Convert.ToSingle(data[i]["CriticalProbability"]),
                            Convert.ToSingle(data[i]["CriticalMultiplier"])
                            );

                        DefinitionList_Database.instance.affixList.Add(o);
                        b = true;
                        break;
                    }
                }
                if (!b)
                    Debug.LogWarning("해당 ID에 맞는 것이 없습니다." + Convert.ToUInt32(data[i]["ID"]));
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    [ContextMenu("Read_MapList")]
    public void Read_MapList()
    {
        List<Dictionary<string, object>> data = CSVReader.Read("MapList");

        if (data.Count != 0)
        {
            List<MapDefinition> list = CreateAsset<MapDefinition>(data.Count, "Assets/10.Scripts/Database/Map/Map");

            DefinitionList_Database.instance.mapList = new List<MapDefinition>();
            for (int i = 0; i < data.Count; i++)
            {
                bool b = false;
                foreach (MapDefinition o in list)
                {
                    if (o.ID == Convert.ToUInt32(data[i]["ID"]))
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(o), Convert.ToString(data[i]["ScriptName"]) + ".asset");
                        o.Name = Convert.ToString(data[i]["Name"]);
                        o.description = Convert.ToString(data[i]["Description"]);

                        //todo : 인트로, 맵아이템
                        o.energy = Convert.ToInt32(data[i]["Energy"]);
                        o.level = Convert.ToInt32(data[i]["Level"]);

                        DefinitionList_Database.instance.mapList.Add(o);
                        b = true;
                        break;
                    }
                }
                if (!b)
                    Debug.LogWarning("해당 ID에 맞는 것이 없습니다." + Convert.ToUInt32(data[i]["ID"]));
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    [ContextMenu("Read_EnemyList")]
    public void Read_EnemyList()
    {
        List<Dictionary<string, object>> data = CSVReader.Read("EnemyList");

        if (data.Count != 0)
        {
            List<EnemyDefinition> list = CreateAsset<EnemyDefinition>(data.Count, "Assets/10.Scripts/Database/Enemy/Enemy");

            DefinitionList_Database.instance.enemyList = new List<EnemyDefinition>();
            for (int i = 0; i < data.Count; i++)
            {
                bool b = false;
                foreach (EnemyDefinition o in list)
                {
                    if (o.ID == Convert.ToUInt32(data[i]["ID"]))
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(o), Convert.ToString(data[i]["ScriptName"]) + ".asset");
                        o.Name = Convert.ToString(data[i]["Name"]);
                        //enemyList[i].description = Convert.ToString(data[i]["Description"]);

                        o.status = new Status
                            (
                            //todo : 어택타입
                            Convert.ToInt32(data[i]["MaxHealth"]),
                            Convert.ToInt32(data[i]["Damage"]),
                            Convert.ToInt32(data[i]["Defence"]),
                            Convert.ToSingle(data[i]["AttackSpeed"]),
                            Convert.ToSingle(data[i]["CriticalProbability"]),
                            Convert.ToSingle(data[i]["CriticalMultiplier"])
                            );

                        DefinitionList_Database.instance.enemyList.Add(o);
                        b = true;
                        break;
                    }
                }
                if (!b)
                    Debug.LogWarning("해당 ID에 맞는 것이 없습니다." + Convert.ToUInt32(data[i]["ID"]));
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    [ContextMenu("Read_OrderList")]
    public void Read_OrderList()
    {
        List<Dictionary<string, object>> data = CSVReader.Read("OrderList");

        if (data.Count != 0)
        {
            List<OrderDefinition> list = CreateAsset<OrderDefinition>(data.Count, "Assets/10.Scripts/Database/Order/Order");

            DefinitionList_Database.instance.orderList = new List<OrderDefinition>();
            for (int i = 0; i < data.Count; i++)
            {
                bool b = false;
                foreach (OrderDefinition o in list)
                {
                    if (o.ID == Convert.ToUInt32(data[i]["ID"]))
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(o), Convert.ToString(data[i]["ScriptName"]) + ".asset");
                        o.Name = Convert.ToString(data[i]["Name"]);
                        //enemyList[i].description = Convert.ToString(data[i]["Description"]);

                         o.OrderType = EnumDatabase.GetEnumValue<OrderType>(Convert.ToString(data[i]["ScriptName"]));
                                                
                        Status tStatus = new Status
                            (
                            //todo : 어택타입
                            Convert.ToInt32(data[i]["MaxHealth"]),
                            Convert.ToInt32(data[i]["Damage"]),
                            Convert.ToInt32(data[i]["Defence"]),
                            Convert.ToSingle(data[i]["AttackSpeed"]),
                            Convert.ToSingle(data[i]["CriticalProbability"]),
                            Convert.ToSingle(data[i]["CriticalMultiplier"])
                            );
                        
                        o.Item = new ItemInfo(Convert.ToString(data[i]["ItemName"]), tStatus);

                        DefinitionList_Database.instance.orderList.Add(o);
                        b = true;
                        break;
                    }
                }
                if (!b)
                    Debug.LogWarning("해당 ID에 맞는 것이 없습니다." + Convert.ToUInt32(data[i]["ID"]));
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    [ContextMenu("Read_CustomerList")]
    public void Read_CustomerList()
    {
        List<Dictionary<string, object>> data = CSVReader.Read("CustomerList");

        if (data.Count != 0)
        {
            List<CustomerDefinition> list = CreateAsset<CustomerDefinition>(data.Count, "Assets/10.Scripts/Database/Customer/Customer");

            DefinitionList_Database.instance.customerList = new List<CustomerDefinition>();
            for (int i = 0; i < data.Count; i++)
            {
                bool b = false;
                foreach (CustomerDefinition o in list)
                {
                    if (o.ID == Convert.ToUInt32(data[i]["ID"]))
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(o), Convert.ToString(data[i]["ScriptName"]) + ".asset");
                        o.Name = Convert.ToString(data[i]["Name"]);
                        //enemyList[i].description = Convert.ToString(data[i]["Description"]);

                        o.OrderType = EnumDatabase.GetEnumValue<OrderType>(Convert.ToString(data[i]["ScriptName"]));
                        o.AddOrder();

                        DefinitionList_Database.instance.customerList.Add(o);
                        b = true;
                        break;
                    }
                }
                if (!b)
                    Debug.LogWarning("해당 ID에 맞는 것이 없습니다." + Convert.ToUInt32(data[i]["ID"]));
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    [ContextMenu("Read_Map_Enemy Relation")]
    public void Read_Map_Enemy_Relation()
    {
        foreach(MapDefinition m in DefinitionList_Database.instance.mapList)
        {
            m.ResetEnemyList();
        }

        List<Dictionary<string, object>> data = CSVReader.Read("Map_Enemy Relation");

        if (data.Count != 0)
        {
            for (int i = 0; i < data.Count; i++)
            {
                MapDefinition tMap = DefinitionList_Database.instance.GetMapByName(Convert.ToString(data[i]["Map"]));
                EnemyDefinition tEnemy = DefinitionList_Database.instance.GetEnemyByName(Convert.ToString(data[i]["Enemy"]));

                tMap.AddEnemy(tEnemy);
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    [ContextMenu("Read_Map_Map Relation")]
    public void Read_Map_Map_Relation()
    {
        foreach (MapDefinition m in DefinitionList_Database.instance.mapList)
        {
            m.ResetLinkedMapList();
        }
        List<Dictionary<string, object>> data = CSVReader.Read("Map_Map Relation");

        if (data.Count != 0)
        {
            for (int i = 0; i < data.Count; i++)
            {
                MapDefinition tStartMap = DefinitionList_Database.instance.GetMapByName(Convert.ToString(data[i]["StartMap"]));
                MapDefinition tEndMap = DefinitionList_Database.instance.GetMapByName(Convert.ToString(data[i]["EndMap"]));

                tStartMap.AddLinkedMap(tEndMap);
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    [ContextMenu("Read_Item_Affix Relation")]
    public void Read_Item_Affix_Relation()
    {
        foreach (ItemDefinition m in DefinitionList_Database.instance.itemList)
        {
            m.ResetAffixList();
        }
        List<Dictionary<string, object>> data = CSVReader.Read("Item_Affix Relation");

        if (data.Count != 0)
        {
            for (int i = 0; i < data.Count; i++)
            {
                ItemDefinition tItem = DefinitionList_Database.instance.GetItemByName(Convert.ToString(data[i]["Item"]));
                AffixDefinition tAffix = DefinitionList_Database.instance.GetAffixByName(Convert.ToString(data[i]["Affix"]));
                
                // Deco = 0, Craft = 1
                if ((int)tItem.ItemType_Refined != (int)tAffix.affixType)
                {
                    Debug.LogWarning("재료 아이템에 다른 타입의 affix가 연결되어 있습니다.\n재료아이템 타입 : " + tItem.ItemType_Refined.ToString() + "\n어픽스 타입 : " + tAffix.affixType.ToString());
                    continue;
                }
                tItem.AddAffix(tAffix);
            }
        }
        else
        {
            Debug.LogWarning("데이터가 없습니다.");
        }
    }

    //[ContextMenu("Read_Customer_Order Relation")]
    //public void Read_Customer_Order_Relation()
    //{
    //    List<Dictionary<string, object>> data = CSVReader.Read("Customer_Order Relation");

    //    if (data.Count != 0)
    //    {
    //        for (int i = 0; i < data.Count; i++)
    //        {
    //            OrderDefinition tOrder = DefinitionList_Database.instance.GetOrderByName(Convert.ToString(data[i]["Order"]));
    //            Debug.Log(Convert.ToString(data[i]["Order"]));
    //            if (Convert.ToString(data[i]["Customer"]) == "일반")
    //            {
    //                CustomerDefinition.AddGeneralOrder(tOrder);
    //                continue;
    //            }

    //            CustomerDefinition tCustomer = DefinitionList_Database.instance.GetCustomerByName(Convert.ToString(data[i]["Customer"]));
    //            tCustomer.AddOrder(tOrder);
    //        }
    //    }
    //    else
    //    {
    //        Debug.LogWarning("데이터가 없습니다.");
    //    }
    //}

    private List<T> CreateAsset<T>(int pDataCount, string pPath) where T : ScriptableObject
    {
        List<T> instanceList = new List<T>();
                
        instanceList = FindInstance.GetAllInstances<T>();
        if (pDataCount > instanceList.Count)
        {
            int dif = pDataCount - instanceList.Count;
            int instanceCount = instanceList.Count;
            Debug.LogWarning(dif + "개의 추가 에셋이 있습니다. 에셋의 ID을 바꿔주세요");

            for (int i = 0; i < dif; i++)
            {
                T instanceDef = ScriptableObject.CreateInstance<T>();
                AssetDatabase.CreateAsset(instanceDef, pPath + "_" + (instanceCount + i) + ".asset");
                instanceList.Add(instanceDef);
            }

            AssetDatabase.SaveAssets();
        }
        else if(pDataCount < instanceList.Count)
        {
            Debug.LogWarning("인스턴스 개수가 맞지 않습니다.");
        }

        RenameToDefalut(instanceList);

        return instanceList;
    }

    private void RenameToDefalut<T>(List<T> list) where T : UnityEngine.Object
    {
        int count = list.Count;
        for(int i = 0; i < count; i++)
        {
            AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(list[i]), i + ".asset");
        }
    }
}
