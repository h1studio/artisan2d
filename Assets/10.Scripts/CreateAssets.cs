﻿//#if UNITY_EIDTOR
using UnityEngine;
using UnityEditor;

public class CreateAssets
{
    [MenuItem("Assets/Create/Ore")]
    public static OreDefinition CreateOre()
    {
        OreDefinition asset = ScriptableObject.CreateInstance<OreDefinition>();

        AssetDatabase.CreateAsset(asset, "Assets/Ore.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }


    [MenuItem("Assets/Create/Item")]
    public static ItemDefinition CreateItem()
    {
        ItemDefinition asset = ScriptableObject.CreateInstance<ItemDefinition>();

        AssetDatabase.CreateAsset(asset, "Assets/Item.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }

    [MenuItem("Assets/Create/Frame")]
    public static FrameDefinition CreateFrame()
    {
        FrameDefinition asset = ScriptableObject.CreateInstance<FrameDefinition>();

        AssetDatabase.CreateAsset(asset, "Assets/Frame.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }
}
//#endif