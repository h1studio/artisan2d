﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Status : IStatus
{
    public Status(IStatus pStatus)
    {
        MaxHealth = new Stat(pStatus.MaxHealth.Value);
        Damage = new Stat(pStatus.Damage.Value);
        Defence = new Stat(pStatus.Defence.Value);
        AttackSpeed = new Stat(pStatus.AttackSpeed.Value);
        CriticalProbability = new Stat(pStatus.CriticalProbability.Value);
        CriticalMultiplier = new Stat(pStatus.CriticalMultiplier.Value);
    }
    public Status(int pMaxHealth, int pDamage, int pDefence, float pAttackSpeed, float pCriticalProbability, float pCriticalMultiplier)
    {
        MaxHealth = new Stat(pMaxHealth);
        Damage = new Stat(pDamage);
        Defence = new Stat(pDefence);
        AttackSpeed = new Stat(pAttackSpeed);
        CriticalProbability = new Stat(pCriticalProbability);
        CriticalMultiplier = new Stat(pCriticalMultiplier);
    }
    
    public IStat MaxHealth { get; protected set; }
    public IStat Damage { get; protected set; }
    public IStat Defence { get; protected set; }
    public IStat AttackSpeed { get; protected set; }
    public IStat CriticalProbability { get; protected set; }
    public IStat CriticalMultiplier { get; protected set; }

    public void AddStatus(IStatus pStatus)
    {
        MaxHealth.AddStat(pStatus.MaxHealth);
        Damage.AddStat(pStatus.Damage);
        Defence.AddStat(pStatus.Defence);
        AttackSpeed.AddStat(pStatus.AttackSpeed);
        CriticalProbability.AddStat(pStatus.CriticalProbability);
        CriticalMultiplier.AddStat(pStatus.CriticalMultiplier);
    }
    public void RemoveStatus(IStatus pStatus)
    {
        MaxHealth.RemoveStat(pStatus.MaxHealth);
        Damage.RemoveStat(pStatus.Damage);
        Defence.RemoveStat(pStatus.Defence);
        AttackSpeed.RemoveStat(pStatus.AttackSpeed);
        CriticalProbability.RemoveStat(pStatus.CriticalProbability);
        CriticalMultiplier.RemoveStat(pStatus.CriticalMultiplier);
    }
    public void SetLevel(int pLevel)
    {
        MaxHealth.Level = pLevel;
        Damage.Level = pLevel;
        Defence.Level = pLevel;
        AttackSpeed.Level = pLevel;
        CriticalProbability.Level = pLevel;
        CriticalMultiplier.Level = pLevel;
    }


    public event Action<Status> OnStatusChanged;

    private void Notify_StatusChanged()
    {
        if (OnStatusChanged != null)
            OnStatusChanged(this);
    }
}

