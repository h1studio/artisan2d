﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FrameDefinition : ScriptableObject
{
    [SerializeField]
    private uint _id;
    public uint ID
    {
        get { return _id; }
        set { _id = value; }
    }

    [SerializeField]
    private string _name = "";
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }
}
