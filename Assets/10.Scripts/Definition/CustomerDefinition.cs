﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CustomerDefinition : ScriptableObject
{

    [Header("MapDefinition")]
    [SerializeField]
    private uint _id;
    public uint ID
    {
        get { return _id; }
        set { _id = value; }
    }

    [SerializeField]
    private string _name = "";
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private string _description = "";
    public virtual string description
    {
        get { return _description; }
        set { _description = value; }
    }

    [SerializeField]
    private OrderType _orderType;
    public OrderType OrderType
    {
        get { return _orderType; }
        set { _orderType = value; }
    }

    [SerializeField]
    public List<OrderDefinition> orderList = new List<OrderDefinition>();
    public void AddOrder()
    {
        foreach (OrderDefinition tOrder in DefinitionList_Database.instance.orderList)
        {
            if (tOrder.OrderType == OrderType.General)
                AddOrder(tOrder);
            else if (tOrder.OrderType == OrderType)
                AddOrder(tOrder);
        }
    }
    private void AddOrder(OrderDefinition pOrder)
    {
        if (!orderList.Contains(pOrder))
            orderList.Add(pOrder);
    }

    //todo: 변경하기
    public OrderDefinition GetOrder()
    {
        return orderList[Random.Range(0, orderList.Count)];
    }
}
