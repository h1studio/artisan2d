﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapDefinition : ScriptableObject {

    [Header("MapDefinition")]
    [SerializeField]
    private uint _id;
    public uint ID
    {
        get { return _id; }
        set { _id = value; }
    }

    [SerializeField]
    private string _name = "";
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private string _description = "";
    public virtual string description
    {
        get { return _description; }
        set { _description = value; }
    }

    [SerializeField]
    private int _energy = 1;
    public int energy
    {
        get { return _energy; }
        set { _energy = value; }
    }

    [SerializeField]
    private int _level = 1;
    public int level
    {
        get { return _level; }
        set { _level = value; }
    }

    [SerializeField]
    private string _introText;
    public string IntroText
    {
        get { return _introText; }
        set { _introText = value; }
    }

    [SerializeField]
    private AdventureType _adventureType;
    public AdventureType AdventureType
    {
        get { return _adventureType; }
        set { _adventureType = value; }
    }

    //todo : enemyList 적용
    //todo : mapList 적용
    public List<EnemyDefinition> enemyList;
    public void AddEnemy(EnemyDefinition pEnemy)
    {
        if (!enemyList.Contains(pEnemy))
            enemyList.Add(pEnemy);
    }
    public void ResetEnemyList()
    {
        enemyList = new List<EnemyDefinition>();
    }

    public List<MapDefinition> linkedMapList;
    public void AddLinkedMap(MapDefinition pMap)
    {
        if (!linkedMapList.Contains(pMap))
            linkedMapList.Add(pMap);
    }
    public void ResetLinkedMapList()
    {
        linkedMapList = new List<MapDefinition>();
    }
}

public enum AdventureType
{
    None = 0,
    General = 1,
    Boss = 2
}
