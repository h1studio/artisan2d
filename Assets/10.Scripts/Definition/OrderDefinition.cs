﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OrderDefinition : ScriptableObject
{

    [Header("OrderDefinition")]
    [SerializeField]
    private uint _id;
    public uint ID
    {
        get { return _id; }
        set { _id = value; }
    }

    [SerializeField]
    private string _name = "";
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private string _description = "";
    public virtual string Description
    {
        get { return _description; }
        set { _description = value; }
    }

    [SerializeField]
    private ItemInfo _item;
    public ItemInfo Item
    {
        get { return _item; }
        set { _item = value; }
    }

    [SerializeField]
    private OrderType _orderType;
    public OrderType OrderType
    {
        get { return _orderType; }
        set { _orderType = value; }
    }
}
