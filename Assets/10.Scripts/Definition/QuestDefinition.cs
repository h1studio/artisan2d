﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestDefinition : ScriptableObject {

    // 퀘스트 이름
    //퀘스트 설명
    //보상
    //보상아이템
    //완료 조건
    public string conditionName;
    public float conditionValue;

}
