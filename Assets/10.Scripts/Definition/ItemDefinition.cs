﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemDefinition : ScriptableObject
{
    [Header("ItemDefinition")]
    [SerializeField]
    private uint _id;
    public uint ID
    {
        get { return _id; }
        set { _id = value; }
    }

    [SerializeField]
    private string _name = "";
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private string _description = "";
    public virtual string Description
    {
        get { return _description; }
        set { _description = value; }
    }

    [SerializeField]
    private Sprite _icon;
    public Sprite Icon
    {
        get { return _icon; }
        set { _icon = value; }
    }

    // 필요시 Enum에서 새로운 클래스로 변경
    [SerializeField]
    private ItemType _itemType;
    public ItemType ItemType
    {
        get { return _itemType; }
        set { _itemType = value; }
    }

    [SerializeField]
    private ItemType_Refined _itemType_Refined;
    public ItemType_Refined ItemType_Refined
    {
        get { return _itemType_Refined; }
        set { _itemType_Refined = value; }
    }

    [SerializeField]
    [Range(1, 999)]
    private uint _maxStackSize = 1;
    public uint MaxStackSize
    {
        get { return _maxStackSize; }
        set { _maxStackSize = value; }
    }
    
    //private uint _currentStackSize = 1;
    //public virtual uint currentStackSize
    //{
    //    get { return _currentStackSize; }
    //    set { _currentStackSize = value; }
    //}

    //[SerializeField]
    //private ItemRarity _rarity;
    //public ItemRarity rarity
    //{
    //    get { return _rarity; }
    //    set { _rarity = value; }
    //}
    
    [SerializeField]
    private int _buyPrice;
    public int BuyPrice
    {
        get { return _buyPrice; }
        set { _buyPrice = value; }
    }

    [SerializeField]
    private int _sellPrice;
    public int SellPrice
    {
        get { return _sellPrice; }
        set { _sellPrice = value; }
    }

    [SerializeField]
    private bool _isSellable = true;
    public bool IsSellable
    {
        get { return _isSellable; }
        set { _isSellable = value; }
    }

    //public virtual void GetInfo()
    //{
    //}

    //public virtual bool CanUse()
    //{
    //    return true;
    //}
    //public virtual int Use()
    //{
    //    return 0;
    //}

    [Header("Produce")]
    [SerializeField]
    private bool _canProduce = true;
    public bool CanProduce
    {
        get { return _canProduce; }
        set { _canProduce = value; }
    }

    public Producable producable;
    
    //public FrameDefinition produce_frame
    //{
    //    get { return producable.produce_frame; }
    //}

    //public OreDefinition produce_ore
    //{
    //    get { return producable.produce_ore; }
    //}

    [Header("Fight")]
    [SerializeField]
    private bool _canFight = true;
    public bool canFight
    {
        get { return _canFight; }
        set { _canFight = value; }
    }

    public StatusGenerator statusGenerator;
    //public Status status;

    [Header("Material")]
    public List<AffixDefinition> affixList;
    public void AddAffix(AffixDefinition pAffix)
    {
        if (!affixList.Contains(pAffix))
            affixList.Add(pAffix);
    }
    public void ResetAffixList()
    {
        affixList = new List<AffixDefinition>();
    }
}



