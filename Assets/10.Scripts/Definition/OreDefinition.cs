﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public partial class OreDefinition : ScriptableObject, IEquatable<OreDefinition>
{
    //public OreDefinition(int i)
    //{

    //}

    [SerializeField]
    private int _id;
    public int ID
    {
        get { return _id; }
        set { _id = value; }
    }

    [SerializeField]
    private string _name = "";
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private float _orePurity;
    public float orePurity
    {
        get { return _orePurity; }
        set { _orePurity = value; }
    }

    public Color oreColor = new Color();
    public void SetColor(string colorHex)
    {
        ColorUtility.TryParseHtmlString(colorHex, out oreColor);
    }

    public bool Equals(OreDefinition other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        if (other.GetType() != this.GetType()) return false;
        return ID == other.ID;
    }
}