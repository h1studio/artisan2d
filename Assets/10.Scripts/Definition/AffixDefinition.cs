﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AffixDefinition : ScriptableObject {

    [Header("AffixDefinition")]
    [SerializeField]
    private uint _id;
    public uint ID
    {
        get { return _id; }
        set { _id = value; }
    }

    [SerializeField]
    private string _name = "";
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private string _description = "";
    public virtual string description
    {
        get { return _description; }
        set { _description = value; }
    }

    //[SerializeField]
    //private Sprite _icon;
    //public Sprite icon
    //{
    //    get { return _icon; }
    //    set { _icon = value; }
    //}

    // 필요시 Enum에서 새로운 클래스로 변경
    // 0:장식, 1:세공, 2:각인
    [SerializeField]
    private AffixType _affixType;
    public AffixType affixType
    {
        get { return _affixType; }
        set { _affixType = value; }
    }

    [SerializeField]
    private int _tier;
    public int tier
    {
        get { return _tier; }
        set { _tier = value; }
    }
    
    public Status status;
}
