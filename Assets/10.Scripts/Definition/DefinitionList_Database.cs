﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DefinitionList_Database : ManagerBase<DefinitionList_Database>
{
    public List<OreDefinition> oreList;
    
    public List<ItemDefinition> itemList;
    
    public List<FrameDefinition> frameList;
    
    public List<AffixDefinition> affixList;
    
    public List<MapDefinition> mapList;

    public List<EnemyDefinition> enemyList;

    public List<OrderDefinition> orderList;

    public List<CustomerDefinition> customerList;

    public ItemDefinition GetItemByID(int pItemID)
    {
        foreach (ItemDefinition t in itemList)
        {
            if (t.ID == pItemID)
            {
                return t;
            }
        }
        return null;
    }

    public ItemDefinition GetItemByName(string pName)
    {
        foreach (ItemDefinition t in itemList)
        {
            if (t.Name == pName)
            {
                return t;
            }
        }
        return null;
    }

    public FrameDefinition GetFrameByName(string pName)
    {
        foreach (FrameDefinition t in frameList)
        {
            if (t.Name == pName)
            {
                return t;
            }
        }
        return null;
    }
    public OreDefinition GetOreByID(int pID)
    {
        foreach (OreDefinition t in oreList)
        {
            if (t.ID == pID)
            {
                return t;
            }
        }
        return null;
    }
    public OreDefinition GetOreByName(string pName)
    {
        foreach (OreDefinition t in oreList)
        {
            if (t.Name == pName)
            {
                return t;
            }
        }
        return null;
    }

    public AffixDefinition GetAffixByName(string pName)
    {
        foreach (AffixDefinition t in affixList)
        {
            if (t.Name == pName)
            {
                return t;
            }
        }
        return null;
    }
    public List<AffixDefinition> GetAffixListByType(AffixType pType)
    {
        var list = new List<AffixDefinition>();
        foreach (AffixDefinition t in affixList)
        {
            if (t.affixType == pType)
            {
                list.Add(t);
            }
        }
        return list;
    }

    public MapDefinition GetMapByName(string pName)
    {
        foreach (MapDefinition t in mapList)
        {
            if (t.Name == pName)
            {
                return t;
            }
        }
        return null;
    }

    public EnemyDefinition GetEnemyByName(string pName)
    {
        foreach (EnemyDefinition t in enemyList)
        {
            if (t.Name == pName)
            {
                return t;
            }
        }
        return null;
    }

    public CustomerDefinition GetCustomerByName(string pName)
    {
        foreach (CustomerDefinition t in customerList)
        {
            if (t.Name == pName)
            {
                return t;
            }
        }
        return null;
    }

    public OrderDefinition GetOrderByName(string pName)
    {
        foreach (OrderDefinition t in orderList)
        {
            if (t.Name == pName)
            {
                return t;
            }
        }
        return null;
    }


}
