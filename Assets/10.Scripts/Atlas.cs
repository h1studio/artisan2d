﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atlas : ManagerBase<Atlas> {

    //public delegate void ScheduleDelegate(List<MapInfo> mapSchedule);
    //public ScheduleDelegate Delegate_UpdateUI_Schedule;

    public static bool scheduleMode;
    private MapInfo curMap;
    [SerializeField]
    private MapDefinition startMap;

    private FatherState state;

    private List<MapInfo> mapSchedule = new List<MapInfo>();

    [SerializeField]
    private float maxTimer_Move = 60f;
    private float timer_Move;

    private void Start()
    {
        curMap = MapManager.instance.GetMapByDefinition(startMap);
    }


    public bool SetSchedule(MapInfo pMap)
    {
        //todo : 에너지 확인해야함
        if(CanSetMapInSchedule(pMap))
        {
            mapSchedule.Add(pMap);
            return true;
        }
        return false;
    }
    public void RemoveSchedule(MapInfo pMap)
    {
        mapSchedule.Remove(pMap);
    }
    public bool IsMapInSchedule(MapInfo pMap)
    {
        return mapSchedule.Contains(pMap);
    }

    private bool CanSetMapInSchedule(MapInfo pMap)
    {
        // 예정 목록에 있는지 확인
        if (IsMapInSchedule(pMap))
        {
            Debug.Log("예정되어 있는 맵 입니다.");
            return false;
        }

        MapInfo lastMap;
        // 마지막 예정지/현재 위치 에서 갈 수 있는지 확인
        if (mapSchedule.Count == 0)
            lastMap = curMap;
        else
            lastMap = mapSchedule[mapSchedule.Count - 1];

        return lastMap.IsLinked(pMap);
    }

    private void ScheduleStart()
    {
        if(state == FatherState.Rest)
        {
            StartCoroutine(Schedule());
        }
    }
    IEnumerator Schedule()
    {
        while(mapSchedule.Count > 0)
        {
            // 이동하기
            MoveTo(mapSchedule[0]);
            while (state == FatherState.Move)
            { yield return null; }

            curMap = mapSchedule[0];

            // 모험하기
            Adventure(mapSchedule[0]);
            while (state == FatherState.Adventure)
            { yield return null; }

            mapSchedule.Remove(mapSchedule[0]);
        }
    }

    private void MoveTo(MapInfo pMap)
    {
        state = FatherState.Move;
        //이동 애니메이션

        //타이머 가동
        MoveTimerStart();
    }
    private void Adventure(MapInfo pMap)
    {
        state = FatherState.Adventure;

        //모험 시작
        //todo : 모험 구현하기
        state = FatherState.Rest;
    }
    IEnumerator MoveTimerStart()
    {
        while (timer_Move < maxTimer_Move)
        {
            timer_Move += Time.deltaTime;
            yield return null;
        }

        timer_Move = 0f;
        // 이동 종료
        state = FatherState.Rest;
    }

    //private void UpdateUI_Schedule()
    //{
    //    if (Delegate_UpdateUI_Schedule != null)
    //        Delegate_UpdateUI_Schedule(mapSchedule);
    //}
}
