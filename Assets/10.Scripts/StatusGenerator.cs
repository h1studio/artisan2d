﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatusGenerator {

    [SerializeField] private int _maxHealth = 10;
    [SerializeField] private int _damage = 1;
    [SerializeField] private int _defence;
    [SerializeField] private float _attackSpeed = 1f;
    [SerializeField] private float _criticalProbability = 1f;
    [SerializeField] private float _criticalMultiplier = 1f;

    public StatusGenerator(int pMaxHealth, int pDamage, int pDefence, float pAttackSpeed, float pCriticalProbability, float pCriticalMultiplier)
    {
        _maxHealth = pMaxHealth;
        _damage = pDamage;
        _defence = pDefence;
        _attackSpeed = pAttackSpeed;
        _criticalProbability = pCriticalProbability;
        _criticalMultiplier = pCriticalMultiplier;
    }

    public Status GenerateStatus()
    {
        return new Status(_maxHealth, _damage, _defence, _attackSpeed, _criticalProbability, _criticalMultiplier);
    }
}
