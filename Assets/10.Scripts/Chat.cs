﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Chat {

    public IChattableObject chatPerson;
    public string contents;
    public float time;
    public int fontSize = 14;
    public Color fontColor;

    public ChatType ChatType
    {
        get
        {
            if (chatPerson == (IChattableObject)ObjectManager.instance.player)
                return ChatType.Mine;
            else if (chatPerson == (IChattableObject)ObjectManager.instance.systemChat)
                return ChatType.System;
            else
                return ChatType.General;
        }
    }

    public Chat(IChattableObject pChatPerson, string pContents, Color? pFontColor = null, int pSize = 14)
    {
        chatPerson = pChatPerson;
        contents = pContents;
        time = Time.time;

        fontColor = pFontColor ?? Color.black;
        fontSize = pSize;        
    }
}

public interface IChattableObject
{
    string Name { get; }
    Sprite Icon { get; }

}

public enum ChatType
{
    General = 0,
    Mine = 1,
    System = 2
}

