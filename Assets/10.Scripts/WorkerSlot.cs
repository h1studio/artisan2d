﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkerSlot : MonoBehaviour {

    private Worker worker;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick_Button);
    }

    public void SetWorker(Worker pWorker)
    {
        worker = pWorker;
        GetComponentInChildren<Text>().text = pWorker.Name;
    }

    public void RemoveWorker()
    {
        worker = null;
        GetComponentInChildren<Text>().text = "";
    }

    private void OnClick_Button()
    {
        if (worker != null)
            ObjectManager.instance.workerUI.SetCurWorker(worker);
    }
}
