﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Player : MonoBehaviour, IChattableObject {
    
    public Transform workerList;
    public List<QuestInfo> questList = new List<QuestInfo>();
    public List<Furnace> furnaceList = new List<Furnace>();
    
    public Float_Var money = new Float_Var(0f);
    public Float_Var tesseract = new Float_Var(0f);

    private OreCollection oreCollection = new OreCollection();

    public Float_Var mana = new Float_Var(2f, 2f,1);
    public Inventory inventory;
    
    public event Action<OreCollection> OnOreChanged;
    
    public StatCollection statCollection;
    public Status status;
    public StatusGenerator statusGenerator;

    [SerializeField]
    private string _name;
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }
    public Sprite Icon
    {
        get { return null; }
    }

    private void Start()
    {
        inventory = GetComponent<Inventory>();
        statCollection = new StatCollection();
        status = statusGenerator.GenerateStatus();
        statCollection.AddStatus(status);
    }

    public void AddOre(OreInfo pOreInfo)
    {
        oreCollection.AddOre(pOreInfo);
        Notify_OreAmountChanged();
    }

    public bool ReduceOre(OreInfo pOreInfo)
    {
        bool b = oreCollection.ReduceOre(pOreInfo);
        Notify_OreAmountChanged();
        return b;
    }

    public int GetOreAmount(OreDefinition pOreDefintion)
    {
        return oreCollection.GetOreAmount(pOreDefintion);
    }

    private void Notify_OreAmountChanged()
    {
        if (OnOreChanged != null)
            OnOreChanged(oreCollection);
            
    }
}
