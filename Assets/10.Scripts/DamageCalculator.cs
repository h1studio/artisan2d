﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCalculator {

    static public float Attack(StatCollection pAttackStatCollection, StatCollection pDefenceStatCollection)
    {
        //크리티컬 계산

        float damage = pAttackStatCollection.Damage;
        float defence = pDefenceStatCollection.Defence;

        pDefenceStatCollection.Health.ChangeValue((int)-damage);
        return damage;
    }

}
