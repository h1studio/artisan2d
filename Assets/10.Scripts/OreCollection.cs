﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreCollection
{
    private List<OreInfo> oreList = new List<OreInfo>();

    public void AddOre(OreInfo pOreInfo)
    {
        foreach (OreInfo oreinfo in oreList)
        {
            if (oreinfo.ore.ID == pOreInfo.ore.ID)
            {
                oreinfo.AddOre(pOreInfo);
                return;
            }
        }

        oreList.Add(pOreInfo);
    }

    public bool ReduceOre(OreInfo pOreInfo)
    {
        foreach (OreInfo oreinfo in oreList)
        {
            if (oreinfo.ore.ID == pOreInfo.ore.ID)
            {
                return oreinfo.ReduceOre(pOreInfo);
            }
        }
        return false;
    }

    public int GetOreAmount(OreDefinition pOreDefinition)
    {
        foreach (OreInfo oreinfo in oreList)
        {
            if (oreinfo.ore.ID == pOreDefinition.ID)
            {
                return oreinfo.amount.Value;
            }
        }
        return 0;
    }
    public int GetOreAmount(string pOreName)
    {
        foreach (OreInfo oreinfo in oreList)
        {
            if (oreinfo.ore.Name == pOreName)
            {
                return oreinfo.amount.Value;
            }
        }
        return 0;
    }
}
