﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour {

    [SerializeField]
    private bool closeWhenStart = true;
    
    private void Awake()
    {
        transform.position = new Vector3(Screen.width/2, Screen.height/2, 0);
        gameObject.SetActive(!closeWhenStart);
    }    
}
