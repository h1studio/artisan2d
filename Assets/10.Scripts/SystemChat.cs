﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemChat : MonoBehaviour, IChattableObject
{
    public string Name
    {
        get { return "시스템"; }
    }
    public Sprite Icon
    {
        get { return null; }
    }
}