﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Float_Var {

    

    //[Range(0,2)]
    private int maxControllMethod = 0;  // 0: max 존재하지 않음, 1: max 넘을시 넣을 수 있는 양만 추가 후 나머지 리턴, 2: max 넘을시 추가 X
    private float defaultValue = 0f;
    
    public Float_Var(float pValue)
    {
        defaultValue = pValue;
        Value = pValue;
        maxControllMethod = 0;
    }
    /// <summary>
    /// pMaxController - 0: max 존재하지 않음, 1: max 넘을시 넣을 수 있는 양만 추가 후 나머지 리턴, 2: max 넘을시 추가 X
    /// </summary>
    /// <param name="pName"></param>
    /// <param name="pValue"></param>
    /// <param name="pMaxValue"></param>
    /// <param name="pMaxControllMethod"></param>
    public Float_Var(float pValue, float pMaxValue, int pMaxControllMethod)
    {
        defaultValue = pValue;
        Value = pValue;
        MaxValue = pMaxValue;
        maxControllMethod = pMaxControllMethod;
    }

    [SerializeField]
    private float _value = 0f;
    public float Value
    {
        get { return _value; }

        private set
        {
            _value = value;
            Notify_VarChanged();
        }
    }

    [SerializeField]
    private float _maxValue = 100f;
    public float MaxValue
    {
        get { return _maxValue; }
        set
        {
            _maxValue = value;
            Notify_VarChanged();
        }
    }

    public float ValueFactor
    {
        get { return Value / MaxValue; }
    }
    public void SetValue(float changeBy)
    {
        Value = changeBy;
    }

        public float ChangeValue(float changeBy)
    {
        if (Value > MaxValue)
            return 0f;

        if (Value + changeBy >= MaxValue)
        {
            switch (maxControllMethod)
            {
                case 0:         // max 존재하지 않음
                    Value += changeBy;
                    return 0f;

                case 1:         // 넣을 수 있는 양만 추가 후 나머지 리턴
                    float returnValue = Value + changeBy - MaxValue;
                    Value = MaxValue;                    
                    return returnValue;

                case 2:         // 추가 X
                    return changeBy;
                    
                default:
                    Debug.LogWarning("maxController 수치가 이상합니다. maxController : " + maxControllMethod);
                    break;
            }
        }
        else
        {
            Value += changeBy;            
        }
        return 0f;

    }

    public void ResetValue()
    {
        Value = defaultValue;
    }

    protected virtual void Start()
    {
        Notify_VarChanged();
    }

    public event Action<Float_Var> OnVarChanged;

    private void Notify_VarChanged()
    {
        if (OnVarChanged != null)
            OnVarChanged(this);
    }
}
