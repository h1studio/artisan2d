﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Craft : MonoBehaviour, ISetItemFromInventory
{
    public ItemInfo item { get; set; }
    public ItemInfo decoMaterial { get; set; }
    public ItemInfo craftMaterial { get; set; }

    public delegate void ItemDelegate();
    public ItemDelegate Delegate_UpdateUI_CraftItem;

    public void SetItem(ItemInfo pItem)
    {
        if (pItem.Type == ItemType.Equipment)
            item = pItem;
        else if(pItem.Type == ItemType.Material)
        {
            if (pItem.RefinedType == ItemType_Refined.Material_Decorate)
                decoMaterial = pItem;
            else if (pItem.RefinedType == ItemType_Refined.Material_Craft)
                craftMaterial = pItem;
        }
        
        Notify_CraftItemChanged();
    }
    public void SetTarget()
    {
        ObjectManager.instance.player.inventory.SetTarget(this);
    }

    public void RemoveItem()
    {
        item = null;
        Notify_CraftItemChanged();
    }

    private void Notify_CraftItemChanged()
    {
        if (Delegate_UpdateUI_CraftItem != null)
            Delegate_UpdateUI_CraftItem();
    }

    public void DecorateItem()
    {
        if(item == null)
        {
            Debug.Log("아이템이 없습니다");
            return;
        }
        else if(decoMaterial == null)
        {
            Debug.Log("재료 아이템이 없습니다");
            return;
        }
        else if(item.GetAffix(1) != null)
        {
            Debug.Log("장식이 되어 있습니다");
            return;
        }

        // 어픽스 적용
        AffixManager.instance.SetFirstAffixOnItem(item, decoMaterial);
                
    }

    public void CraftItem()
    {
        if (item == null)
        {
            Debug.Log("아이템이 없습니다");
            return;
        }
        else if (craftMaterial == null)
        {
            Debug.Log("재료 아이템이 없습니다");
            return;
        }

        // 어픽스 적용
        AffixManager.instance.SetSecondAffixOnItem(item, craftMaterial);
    }

    public void EngraveItem()
    {

    }
}
