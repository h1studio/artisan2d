﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour {
    
    [SerializeField]
    private float customerDelay;    // 손님 쿨타임
    private Timer customerTimer;    // 손님 쿨타임 타이머
    
    [SerializeField]
    private Transform[] customerPosition;   // 손님 위치    
    
    [SerializeField]
    private int customerCountMax;       // 손님 최대 수
    private List<Customer> customerList = new List<Customer>();
    
    //todo:손님 프리팹 리소스에서 받기    
    private static int curOrderCount;      // 주문서 납품횟수 누적

    private Customer curCustomer;

    [Header("UI")]
    [SerializeField]
    private Text text_Timer;

    private void Awake()
    {
        customerTimer = gameObject.AddComponent<Timer>();
        customerTimer.Init(customerDelay, ReceiveCustomer, UpdateUI_Timer);
        customerTimer.StartTimer();
    }
    
    private void ReceiveCustomer()
    {
        var customer = CustomerManager.instance.GetRestCustomerByRandom();     // 어떤 손님 받을지 선택
        if (customer == null)
            Debug.LogWarning("손님 수가 너무 적은듯..");

        customerList.Add(customer);
                
        customer.EnterShop(customerPosition[customerList.Count - 1].position);       // 손님 이동

        if (!CanReceiveCustomer)
            customerTimer.StopTimer();
        else if (!customerTimer.isTimerRunning)
            customerTimer.StartTimer();        
    }
    public void OutCustomer(Customer pCustomer)
    {
        customerList.Remove(pCustomer);        
        pCustomer.OutShop();
        if (curCustomer == pCustomer)
            curCustomer = null;

        AlignCustomerPosition();

        if(!customerTimer.isTimerRunning)
            customerTimer.StartTimer();            
    }

    private bool CanReceiveCustomer
    {
        get { return customerList.Count < customerCountMax; }
    }

    public void AlignCustomerPosition()
    {
        for(int i=0; i < customerList.Count; i++)
        {
            customerList[i].MovePosition(customerPosition[i].position);
        }
    }

    public void SetCurCustomer(Customer pCustomer)
    {
        if(curCustomer != null)
        {
            curCustomer.ToggleOrderSelectUI(false);
        }

        curCustomer = pCustomer;
    }

    public Customer GetOrderCustomer(Order pOrder)
    {
        foreach (Customer c in customerList)
        {
            if (c.Order == pOrder)
                return c;
        }

        Debug.LogWarning("해당 주문의 손님이 없습니다.");
        return null;
    }


    #region UI
    private void UpdateUI_Timer()
    {
        if (customerTimer.isTimerRunning)
        {
            float remainTime = customerTimer.MaxValue - customerTimer.Value;
            text_Timer.text = TS_Math.GetMin(remainTime) + ":" + TS_Math.Round(TS_Math.GetSec(remainTime), 0);
        }
        else
        {
            text_Timer.text = "Max";            
        }
    }

    #endregion



}
