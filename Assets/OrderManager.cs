﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class OrderManager {

    public static void Deliver(Order pOrder)
    {
        var customer = ObjectManager.instance.shop.GetOrderCustomer(pOrder);
        ObjectManager.instance.shop.OutCustomer(customer);

        Reward();
    }
    public static void GiveUp(Order pOrder)
    {
        var customer = ObjectManager.instance.shop.GetOrderCustomer(pOrder);
        ObjectManager.instance.shop.OutCustomer(customer);

        //RemoveCustomer();
        //RemovePlayerItem();
    }

    private static void Reward()
    {
        //todo:보상 구현
        Debug.Log("보상 1원~");
    }


}
