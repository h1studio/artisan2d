﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager : ManagerBase<PanelManager> {

    [SerializeField]
    private Camera cam;

    [SerializeField] private GameObject minePanel;    
    [SerializeField] private GameObject smeltPanel;
    [SerializeField] private GameObject forgingPanel;

    [SerializeField] private Transform mineCameraPosition;
    [SerializeField] private Transform smeltCameraPosition;
    [SerializeField] private Transform forgingCameraPosition;

    [SerializeField] private GameObject inventory;


    [SerializeField] private GameObject workerPanel;
    
    public void OpenMinePanel()
    {
        CloseOtherPanel();

        cam.transform.position = mineCameraPosition.position;
        minePanel.SetActive(true);
    }
    public void OpenSmeltPanel()
    {
        CloseOtherPanel();

        cam.transform.position = smeltCameraPosition.position;
        smeltPanel.SetActive(true);
    }
    public void OpenForgingPanel()
    {
        CloseOtherPanel();

        cam.transform.position = forgingCameraPosition.position;
        forgingPanel.SetActive(true);
    }

    public void OpenWorkerPanel()
    {
        CloseOtherPanel();

        workerPanel.SetActive(true);
    }

    private void CloseOtherPanel()
    {
        minePanel.SetActive(false);
        smeltPanel.SetActive(false);
        forgingPanel.SetActive(false);
        workerPanel.SetActive(false);
    }

    public void OpenInventory()
    {
        CloseOtherPanel();

        inventory.SetActive(true);
    }
}
