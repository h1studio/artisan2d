﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreObject : MonoBehaviour {

    [SerializeField]
    private float destroyTime;

    private void Start()
    {
        Destroy(this.gameObject, destroyTime);
    }

    private void OnDestroy()
    {
        GetOre();
    }

    private void GetOre()
    {
        var ore = new OreInfo("구리 광석");
        ObjectManager.instance.player.AddOre(ore);
    }
}
