﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Stat : IStat {
    
    public Stat(float pValueRaw, float pLocalFactor = 1f, float pGlobalFactor = 1f)
    {
        ValueRaw = pValueRaw;

        LocalFactor = pLocalFactor;
        GlobalFactor = pGlobalFactor;
        Level = 1;
        LevelIndex = 1f;
    }
    
    public float ValueRaw { get; protected set; }
    public float Value { get { return (Mathf.Clamp(ValueRaw, float.MinValue, float.MaxValue) ); } }
    public float ValueNotClamped { get { return ValueRaw * LocalFactor * LevelFactor; } }

    public float LocalFactor { get; protected set; }
    public float GlobalFactor { get; protected set; }

    private float LevelFactor { get { return Mathf.Pow(LevelIndex, Level); } }
    public int Level { get; set; }
    public float LevelIndex { get; protected set; }
    
    public void AddStat(IStat pStat)
    {
        ValueRaw += pStat.ValueRaw;
        LocalFactor *= pStat.LocalFactor;
        GlobalFactor *= pStat.GlobalFactor;
    }
    public void RemoveStat(IStat pStat)
    {
        ValueRaw -= pStat.ValueRaw;
        LocalFactor /= pStat.LocalFactor;
        GlobalFactor /= pStat.GlobalFactor;
    }

    public void AddStat(float pValue, float pLocalFactor = 1f, float pGlobalFactor = 1f)
    {
        ValueRaw += pValue;
        LocalFactor *= pLocalFactor;
        GlobalFactor *= pGlobalFactor;
    }

    //public event Action<Stat> OnStatChanged;

    //private void Notify_StatChanged()
    //{
    //    if (OnStatChanged != null)
    //        OnStatChanged(this);
    //}
}
