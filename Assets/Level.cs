﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class LevelInfo {

//    public delegate void LevelDelegate(LevelInfo level);
//    public LevelDelegate Delegate_Notify_ExpChanged;

//    private int[] cumExpList;
//    private Int_Var exp;

//    public LevelInfo(int[] pCumExpList = null, int pMaxLevel = 1000)
//    {
//        exp = new Int_Var(0);
//        exp.OnVarChanged += Notify_ExpChanged;

//        if(pCumExpList != null)
//            cumExpList = pCumExpList;
//        else
//        {
//            cumExpList = new int[pMaxLevel];
//            for(int i=0; i < cumExpList.Length; i++)
//            {
//                cumExpList[i] = i;
//            }            
//        }            
//    }

//    public int Level { get { return GetLevel(); } }
//    public int Exp { get { return cumExpList[Level+1] - cumExpList[Level]; } }
//    public int CurExp { get { return exp.Value - cumExpList[Level]; } }
//    public int RemainExp { get { return cumExpList[Level+1] - exp.Value; } }

//    public void SetExp(int pExp)
//    {
//        exp.SetValue(pExp);
//    }
//    public void ChangeExp(int pExp)
//    {
//        exp.ChangeValue(pExp);
//    }
//    public void LevelUp()
//    {
//        ChangeExp(RemainExp);
//    }

//    private int GetLevel()
//    {
//        for(int i=1; i < cumExpList.Length; i++)
//        {
//            if (exp.Value < cumExpList[i+1])
//                return i;
//        }
//        return 0;
//    }

//    protected virtual void Start()
//    {
//        Notify_ExpChanged(null);
//    }

//    private void Notify_ExpChanged(Int_Var p)
//    {
//        if (Delegate_Notify_ExpChanged != null)
//            Delegate_Notify_ExpChanged(this);
//    }
//}
