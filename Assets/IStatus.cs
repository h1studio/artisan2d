﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface IStatus {

    IStat MaxHealth { get;}
    IStat Damage { get; }
    IStat Defence { get; }
    IStat AttackSpeed { get; }
    IStat CriticalProbability { get; }
    IStat CriticalMultiplier { get; }

    void AddStatus(IStatus pStatus);
    void RemoveStatus(IStatus pStatus);
    void SetLevel(int pLevel);

    event Action<Status> OnStatusChanged;

    //void Notify_StatusChanged();
}
